# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#

import sphinx_rtd_theme  # CC
# import os  # CC
# import sphinx_rtd_theme
# import sys  # CC


# sys.path.insert(0, os.path.abspath('../..'))  # CC


# -- Project information -----------------------------------------------------

project = 'ARA User Manual'
copyright = '© 2021, ARA Team'
author = 'ARA Team'

# The full version, including alpha/beta/rc tags
release = '0.1'


# -- General configuration ---------------------------------------------------

# master_doc = 'index' ## CC

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']  # CC

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# START COMMENT
# html_theme_options = {
#     'logo_only': False,
#     'display_version': False,
# }
# END COMMENT


# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
# html_theme = 'alabaster'
html_static_path = ['_static']
html_theme = 'sphinx_rtd_theme'
# html_logo = '_static/ARA_Logo_2.png'
html_baseurl = '/ara-user-manual/'  # CC

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".

numpydoc_show_class_members = False  # CC
