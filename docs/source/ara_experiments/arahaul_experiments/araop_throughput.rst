.. _AraOptical_Experiment_Link_Latency_Throuhput:

AraOptical (FSOC) Link Latency and Throughput Measurement 
-----------------------------------------------------------------

**Resources needed:** AraOptical link and AraHaul host servers.

**Detailed Description:** The experiment allows users to measure the
latency and throughput of the AraOptical link. The experiment setup is
illustrated below.

.. figure:: images/AraOp_Exp1.png
   :align: center

Before proceeding with the experiment, ensure that the AraOptical link
between Wilson Hall and Agronomy Farm is shown as **Available** in the
*Resource Overview* page (under the *AraHaul Links* section, in the
*Optical* column) in the ARA Portal as indicated below.

.. figure:: images/Optical_Link_Available.png
   :align: center
   :width: 800


**Detailed Steps for the Experiment**

1. Login to the portal `<portal.arawireless.org>`_  with login
   credentials.

2. Create two reservations using the *Project -> Reservations ->
   Leases* tab from the dashboard.

      1. Arahaul Host Computer at Wilson Hall

	 * *Site*: Wilson Hall  
	 * *Resource Type*: AraHaul  
	 * *Device Type*: Host
	 * *Device ID*: 000

      2. AraHaul Host Computer at Agronomy Farm

	 * *Site*: Agronomy Farm
	 * *Resource Type*: AraHaul
	 * *Device Type*: Host
	 * *Device ID*: 000

3. Launch containers, using *Project -> Containers* tab in the
   dashboard, on **both** the reserved nodes. For containers, use the
   following attributes. 

   * *Container Image*: ``arawirelesshub/perf:optical``
   * *Network*: ARA_Shared_Net

     
   Provide other attributes as per your discretion following the
   :ref:`ARA Hello World Experiment <ARA_Hello_World>`.

4. Once the container is launched, a floating IP will automatically
   associated with each container. The floating IP allows you to
   access the container remotely through SSH via ARA jumpbox. Visit
   :ref:`ARA_Jumpbox` for more information on accessing containers via
   jumpbox.


5. On login to the Wilson Hall and Agronomy Farm containers, check the
   local IP addresses of both, which are used in the experiment to
   perform the latency testing. Run the following command for checking
   container IP in both containers. ::

    # ip addr

   Note the IP address of the form **192.168.1.X** in both
   containers. An example is shown below.

   .. figure:: images/Optical_IP_Addr.png
      :align: center


**Latency Measurement**

6. In this experiment, we measure the latency, i.e., the
**Round-Trip-Time (RTT)**, over the FSOC link using the *ping*
utility.
  
   .. Note:: For this experiment, we assume the IP address of
          AraOptical container at Wilson Hall is **192.168.1.128** and
          that of Agronomy Farm is **192.168.1.2**.

   For measuring the RTT from Wilson Hall to Agronomy Farm, execute
   the following command at Wilson Hall container. ::

    # ping 192.168.1.2 -c 10

   The command above sends only 10 packets (specified in the -c
   option) to measure the delay. However, at times, the link may be
   unstable and may not provide enough ping responses. In such cases,
   you can change it to a larger number. An example output from Wilson
   Hall container is shown below.

   .. figure:: images/Optical_WH_Ping.png
      :align: center
    
   For measuring the RTT from Agronomy Farm to Wilson Hall, execute
   the following command at the Agronomy Farm container. ::
    
    # ping 192.168.1.128 -c 10

   .. figure:: images/Optical_AF_Ping_Result.png
      :align: center


**TCP Throughput Measurement**

7. For the throughput measurements, we use the tool *iPerf* over the
   FSOC link. For measurement, we need to set one container as the
   *iPerf* server and the other as the *iPerf* client. In the example
   below, we run the *iPerf* server Agronomy Farm container and the
   *iPerf* client at Wilson Hall.

   Run the following at the Agronomy Farm container to start *iPerf*
   server. ::

    # iperf3 -s

   The *iPerf* server start as follows:

   .. figure:: images/Optical_iperf_Server.png
      :align: center
   

   For measuring the **TCP throughput** from Wilson Hall to Agronomy
   Farm, run the *iPerf* client at Wilson Hall container as
   follows. ::

    # iperf3 -c 192.168.1.2

   A snapshot of the *iPerf* output at Agronomy Farm is shown below. 

   .. figure:: images/Optical_WH_iperf_TCP_Client.png
      :align: center
    
   At the same time, you can see similar output at the the *iPerf*
   server at Wilson Hall as follows:

   .. figure:: images/Optical_WH_iperf_TCP_Server.png
      :align: center

   .. note:: If you face any error in the *iPerf* server side, please
	     check the :ref:`AraHaul FAQ <FAQ_Optical_iperf_Error>`. 


**UDP Throughput Measurement**

9. For measuring UDP, we do the same procedure as TCP. However, in the
   client, we need to enable the UDP option.

   Start the *iPerf* server at the Agronomy Farm container as
   follows. ::

    # iperf3 -s

   Start the *iPerf* client at the Wilson Hall container using the
   following command. ::

    # iperf3 -u -c 192.168.1.2 -b 3000M -t 10
     
   The above command measures the UDP throughput (using the -u option)
   for 10 seconds and produce the output as follows. 

   .. figure:: images/Optical_WH_iperf_UDP_Client.png
      :align: center

   At the same time, the UDP server at Agronomy Farm generates the
   corresponding output as follows:
  
   .. figure:: images/Optical_WH_iperf_UDP_Server.png
      :align: center
    

10. If you want to save the log of the *iPerf* result, use the
    following command. ::

      # iperf3 -c 192.168.1.2 > throughput.txt

    Here a file *throughput.txt* will be created. The file can be
    listed and content can be printed using the following commands.::

     # ls
     # cat throughput.txt
   
    The file *throughput.txt* can be copied to your PC via the ARA
    jumpbox as per the instructions provided :ref:`here
    <Experiment_Data_Collection>`.
   
