.. _AraHaul_Experiment_Satcom_Access:

Measuring OneWeb Satellite Link Characteristics
=============================================================

**Short Description:** Accessing OneWeb satellite link.

**Detailed Description:** The experiment allows you to access the
OneWeb satellite link from an experiment container via a Hughes'
terminal.  In addition, you can measure the latency of the link.

**Detailed Steps for the Experiment**

#. Login to `ARA portal <https://portal.arawireless.org>`_ with your
   credentials.

#. Create a reservation using the *Project -> Reservations -> Leases*
   tab from the dashboard.

      * *Site*: Wilson Hall
      * *Resource Type*: AraHaul
      * *Device Type*: Host
      * *Device ID*: 000

#. Create a container from the *Project -> Container -> Containers*
   tab from the dashboard by clicking the **Create Container** button
   on the top-right. Use the following values in the container
   creation form. You can keep the values of other fields as default.

      * *Name:* Select a name for your container
      * *Image:* ``arawirelesshub/ara-ubuntu:22.04``
      * *Command:* bash
      * *Lease Name:* Provide the name of the lease you create in
	Step 2.
      * *Networks:* ARA_Shared_Net (Click the ↑ button to select the
	network)

   Click on the **Create** button to create the container. 

#. Select your container from the list of containers. The page
   provides the container information including name, status, and
   resource allocation as shown in the figure below. Take a note on
   the **Floating IP Address** of the container (marked in red
   rectangle in the figure below.)

   .. figure:: images/floating_ip.png
      :alt: Floating IP Address location

#. Set up our container for the SSH access. From the **Console** tab
   of our container info page, access a web console. Start the SSH
   service using the following command. ::

     # service ssh restart

   Set the password for the container using the command below.::

     # password
      
#. Access the container using the floating IP address from ARA
   Jumpbox. You can visit :ref:`ARA Jumpbox page <ARA_Jumpbox>` for
   detailed procedures for accessing container from the jumpbox.

#. It will be necessary to create appropriate routes through the
   OneWeb gateway. Though you may wish to limit your routing rule to
   specific addresses or subnets, we'll create a default route for
   this example. Use the following command to do so. ::
        
    # route add 8.8.8.8 gw 192.168.42.1 dev eth1

#. For measuring the delay, run the following command from the
   container. ::

    # ping -I eth1 8.8.8.8
   

If you have any issues or questions, please reach out at
*support@arawireless.org*.
