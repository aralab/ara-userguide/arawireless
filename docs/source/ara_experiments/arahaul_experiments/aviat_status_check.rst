.. _AraHaul_Experiment_Link_Configuration:

Aviat Link Configuration (Microwave and Millimeter-wave) and Status Check 
---------------------------------------------------------------------------------------------------------

**Resources needed:** Aviat Networks mmWave and microwave radios
(WTM 4800) and radio host servers.

**Short Description:** Aviat link configuration and measurement.

**Detailed Description:** The experiment allows users to measure the
configuration parameters of Aviat radios and check related
metrics. The following figure shows the experiment setup involving
a long-range (~6 miles) microwave/millimeter-wave links. 

.. figure:: images/Aviat_Experiment_1.png
   :width: 650
   :align: center
	   
|

Follow the steps below to create leases and launch containers for this
experiment:

1. Login to the portal `<portal.arawireless.org>`_  with login
   credentials.

2. Create two reservations using the *Project -> Reservations ->
   Leases* tab from the dashboard.

      1. Aviat Host Computer at Wilson Hall

	 * *Site*: Wilson Hall  
	 * *Resource Type*: AraHaul  
	 * *Device Type*: Host
	 * *Device ID*: 000

      2. Aviat Host Computer at Agronomy Farm

	 * *Site*: Agronomy Farm
	 * *Resource Type*: AraHaul
	 * *Device Type*: Host
	 * *Device ID*: 000

3. Launch containers, using *Project -> Containers* tab in the
   dashboard, on both the reserved nodes using the corresponding
   reservation IDs. For containers, use the **Container Image** as
   ``arawirelesshub/aviatcli:ssh``. Provide other attributes as per
   your discretion following the :ref:`ARA Hello World Experiment
   <ARA_Hello_World>`.

4. Once the container is launched, note the floating IP to access the
   container remotely through SSH via ARA jumpbox. Visit
   :ref:`ARA_Jumpbox` for more information on accessing containers via
   jumpbox.

5. The containers can be accessed via the **console** tab of the
   respective containers in the *Project -> Containers* tab from the
   dashboard or using SSH via the jumpbox server. 


Checking the Status of Link/Radio
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


6. For retrieving the **carrier capabilities** of the Aviat radio from
   local-end (i.e., measuring the carrier capability of Aviat Radio of
   Wilson Hall from Wilson Hall itself), execute the following command
   in the console of the container launched at Wilson Hall host:

   * For 11 GHz

     ``# aviatcli carrier show capabilities Carrier1/1``

   * For 80 GHz

     ``# aviatcli carrier show capabilities mmwCarrier1/1``

   To check the **carrier capabilities** of radio remotely, i.e.,
   retrieve the capabilities of Wilson Hall Aviat radio from Agronomy
   Farm or vice versa, use the following command in the respective
   container console:

   * For 11 GHz

     ``# aviatcli carrier show capabilities Carrier1/1 --remote``

   * For 80 GHz

     ``# aviatcli carrier show capabilities mmwCarrier1/1 --remote``

   To check the **carrier configuration** status of local-end Aviat
   radio using the command below:

   * For 11 GHz

     ``# aviatcli carrier show status Carrier1/1``

   * For 80 GHz

     ``# aviatcli carrier show status mmwCarrier1/1``

     Example output: ::

       root@198849877b10:/# aviatcli carrier show status mmwCarrier1/1
       
       Welcome to the WTM4800 Multi-band CLI

       admin connected from 10.188.0.131 using ssh on WilsonHall
       WilsonHall# show radio-carrier status mmwCarrier1/1
       radio-carrier status mmwCarrier1/1
       oper-status up
       remote-oper-status      up
       regulatory-standard     ANSI
       bandwidth               2000.0 MHz
       tx-modulation           qam-64
       rx-modulation           qam-64
       current-tx-power        11.5 dBm
       tx-frequency            83500000 kHz
       rx-frequency            73500000 kHz
       tx-rx-spacing           10000000 kHz
       tx-airlink-capacity current 8184.284 Mbps
       tx-airlink-capacity average 7396.952 Mbps
       tx-airlink-capacity maximum 9902.343 Mbps
       rx-airlink-capacity current 8184.284 Mbps
       rx-airlink-capacity average 7791.966 Mbps
       acm-tx-status           enable
       snr                     27.5 dB
       remote-snr              26.3 dB
       rsl                     -47.0 dBm
       remote-rsl              -47.8 dBm
       fade-margin             4.0 dB
       atpc-fade-margin        -1.5 dB
       remote-fade-margin      3.2 dB
       remote-atpc-fade-margin -2.3 dB
       ber                     0.00E+00
       tx-mute                 disable
       power-mode              atpc
       fcc-atpc                disable
       active-rx-time          605237 s
       rx-sync-loss-time       8261 s
       all-bytes               314.25 T
       un-correctable-bytes    183622756608


   To check the carrier configuration status of remote-end Aviat radio
   using the below command

     * For 11 GHz

       ``# aviatcli carrier show status Carrier1/1 --remote``

     * For 80 GHz

       ``# aviatcli carrier show status mmwCarrier1/1 --remote``

   To check the whole **radio capabilities** of Aviat radio locally
   and remotely, the following commands can be used

     ``# aviatcli radio show capabilities``

     Example output ::

       root@41f0d45138c3:/# aviatcli carrier show capabilities

       Welcome to the WTM4800 Multi-band CLI

       admin connected from 10.188.0.131 using ssh on WilsonHall
       WilsonHall# show radio-carrier capabilities Carrier1/1
       radio-carrier capabilities Carrier1/1
       min-tx-frequency           11200000 kHz
       max-tx-frequency           11505000 kHz
       min-rx-frequency           10695000 kHz
       max-rx-frequency           10995000 kHz
       available-min-output-power 9.5 dBm
       available-max-output-power 29.5 dBm
       available-max-acm          qam-4096
       freq-step                  50.0 kHz
       freq-band                  11.0 GHz
       freq-max-edge-to-edge      500000 kHz
       max-airlink-throughput     1062.05 Mbps
       supported-ANSI-bandwidths  [ 3.75 5.0 10.0 20.0 25.0 30.0 40.0 50.0 60.0 75.0 80.0 100.0 ]
       supported-ETSI-bandwidths  [ 7.0 14.0 28.0 40.0 56.0 80.0 100.0 105.0 112.0 ]
       supported-modulations      [ qpsk qam-16 qam-32 qam-64 qam-128 qam-256 qam-512 qam-1024 qam-2048 qam-4096 ]
       min-tx-rx-spacing          205000 kHz
       max-tx-rx-spacing          810000 kHz
       standard-tx-rx-spacings    [ 490000 500000 520000 530000 ]
       custom-tx-rx-spacings      Capable
       idq-optimisation-capable   Capable

      
     ``# aviatcli radio show capabilities --remote``


     Example Output ::

       root@3cc6e67b765e:/# aviatcli radio show capabilities --remote

       Welcome to the WTM4800 Multi-band CLI

       admin connected from 10.188.2.4 using ssh on AgFarm
       AgFarm# show radio-link capabilities

       NAME   XPIC CAPABLE   MIMO CAPABLE  A2C CAPABLE   SPACE DIVERSITY CAPABLE    TX PROTECTION CAPABLE

       Radio1  NOT capable    NOT capable    capable            capable                   NOT capable

   For checking the **radio status** of local and remote ends Aviat
   radio using the below command

     ``# aviatcli radio show status``
   
     ``# aviatcli radio show status --remote``

     Example Output ::

       root@3cc6e67b765e:/# aviatcli radio show status --remote

       Welcome to the WTM4800 Multi-band CLI

       admin connected from 10.188.2.4 using ssh on AgFarm
       AgFarm# show radio-link status
       radio-link status Radio1
       oper-status     up
       mode            two-plus-zero
       mlhc            disable
       a2c             disabled
       xpic            disabled
       mimo            disabled
       space-diversity disabled
       tx-protection   disabled
       attached-carriers [ Carrier1/1 mmwCarrier1/1 ]

Configuring the Link/Radio Parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

7. In addition to the radio/link status check, ARA provides APIs to
   configure the radios and change the link characteristics. The
   following commands provide options for enabling/disabling,
   configuring, and checking the status of the link.
  
.. note:: Before changing the configuration, ensure the existing
	  configuration of the carrier. In addition, if you want to
	  change the carrier from Carrier-A to Carrier-B, strictly
	  follow the sequence: (1) enable Carrier-B; (2) disable
	  Carrier-A. For example, if the current carrier is Carrier1/1
	  and you want to change the Carrier to mmwCarrier1/1, first
	  you should enable mmwCarrier1/1 and disable Carrier1/1.
|
   The basic API usage of Aviat radio configuration is as follows:

     ``aviatcli carrier [OPTIONS] COMMAND [ARGUMENTS] ...``

   For example, for getting help on the OPTIONS and ARGUMENTS, use the
   following command:

     ``aviatcli carrier --help``
   ..
      Arguments:

	 CARRIER: {Carrier1/1|Carrier1/2|mmwCarrier1/1}

      Options: 

	 --help  Show this message and exit.

      Commands:

	| ``configure``  
	| ``disable``    Disable given carrier interface
	| ``enable``     Enable given carrier interface
	| ``mute``       Mute give carrier interface
	| ``reset``      Reset all performance configuration of given carrier interface


.. note:: Provide ``--help`` option to any command to get usage
          information from that level onward.

8. Enable the microwave carrier, i.e., Carrier1/1

   ``# aviatcli carrier enable Carrier1/1``

   Example Output ::

     root@demohost:~# aviatcli carrier enable Carrier1/1

     Welcome to the WTM4800 Multi-band CLI

     admin connected from 10.188.0.131 using ssh on WilsonHall
     WilsonHall# config
     Entering configuration mode terminal
     WilsonHall(config)# interface Carrier1/1
     WilsonHall(config-interface-Carrier1/1)# enabled
     WilsonHall(config-interface-Carrier1/1)# commit


   ``# aviatcli carrier enable Carrier1/1 --remote``

   Example Output ::

     root@demohost:~# aviatcli carrier enable Carrier1/1 --remote

     Welcome to the WTM4800 Multi-band CLI

     admin connected from 10.188.2.4 using ssh on AgFarm
     AgFarm# config
     Entering configuration mode terminal
     AgFarm(config)# interface Carrier1/1
     AgFarm(config-interface-Carrier1/1)# enabled
     AgFarm(config-interface-Carrier1/1)# commit

      
9. Configure the Aviat link characteristics.

   Get help on the provided configuration options with the following
   commands. 

     ``# aviatcli carrier configure --help``

     ``# aviatcli carrier configure all --help``
   ..
      The options provided below can be used to set different
      characteristics of the radio.

       | ``all``         Configure bandwidth, freq, modulation and output power (fixed)
       | ``bandwidth``   Configure the Frequency and Bandwidth of carrier interface
       | ``modulation``  Configure the Modulation of carrier interface
       | ``txpower``     Configure the TX Power of carrier interface


   The basic API usage of Aviat configuration is

     ``aviatcli carrier configure [OPTIONS] COMMAND [ARGUMENTS] ...``

  ..
     .. admonition:: limitation The parameters ``bandwidth``,
		     ``modulation``, and ``txpower`` these standalone
		     commands are not available to use in this
		     experiment.  Please use *all* command to change the
		     configuration and then check the Carrier status.

     ``# aviatcli carrier configure all --help``

     Usage: ``aviatcli carrier configure all [OPTIONS] [ARGS]``

       Configure bandwidth, freq, modulation and output power (fixed)

     Arguments: 

       CARRIER:{Carrier1/1|Carrier1/2|mmwCarrier1/1}  <Name of the carrier  [required]>

     Options: 

       --remote                        Apply to remote aviat node
       -b, --bandwidth FLOAT           Bandwidth value to set  [required]
       -t, --txfreq INTEGER            TX Frequency to set  [required]
       -r, --rxfreq INTEGER            RX Frequency to set  [required]
       -f, --modulation [quarter-qpsk|half-qpsk|qpsk|qam-16|qam-32|qam-64|qam-128|qam-256|qam-512|qam-1024|qam-2048|qam-4096] <Modulation scheme for fixed mode>
       -p, --power FLOAT               Selected output power for RTPC mode
       --help                          Show this message and exit.


     Please try to check the change of the configuration by using the
     below example commands of different combinations. 
  


.. caution:: Take sufficient care while changing the radio parameters.
| 
  Configure radio with the configuration: (a) bandwidth 50MHz, (b)
  transmit frequency 11325000 KHz, (c) receive frequency 10835000
  KHz, (d) modulation of QAM-2048, and (e) transmission power of 26 dBm.

   ``# aviatcli carrier configure all -b 50 -t 11325000 -r 10835000 -f
   qam-2048 -p 26 Carrier1/1``

   Example output ::

    root@demohost:~# aviatcli carrier configure all -b 50 -t 11325000 -r 10835000 -f qam-2048 -p 26 Carrier1/1

    Welcome to the WTM4800 Multi-band CLI

    admin connected from 10.188.0.131 using ssh on WilsonHall
    WilsonHall# config
    Entering configuration mode terminal
    WilsonHall(config)# interface Carrier1/1
    WilsonHall(config-interface-Carrier1/1)# power rtpc selected-output-power 26.0
    WilsonHall(config-interface-Carrier1/1)# bandwidth ANSI 50.0
    WilsonHall(config-interface-Carrier1/1)# tx-frequency 11325000
    WilsonHall(config-interface-Carrier1/1)# rx-frequency 10835000
    WilsonHall(config-interface-Carrier1/1)# coding-modulation Fixed qam-2048
    WilsonHall(config-interface-Carrier1/1)# commit


    Welcome to the WTM4800 Multi-band CLI

    admin connected from 10.188.2.4 using ssh on AgFarm
    AgFarm# config
    Entering configuration mode terminal
    AgFarm(config)# interface Carrier1/1
    AgFarm(config-interface-Carrier1/1)# power rtpc selected-output-power 26.0
    AgFarm(config-interface-Carrier1/1)# bandwidth ANSI 50.0
    AgFarm(config-interface-Carrier1/1)# tx-frequency 10835000
    AgFarm(config-interface-Carrier1/1)# rx-frequency 11325000
    AgFarm(config-interface-Carrier1/1)# coding-modulation Fixed qam-2048
    AgFarm(config-interface-Carrier1/1)# commit
    Commit complete.
    AgFarm(config-interface-Carrier1/1)#
    root@demohost:~#
|  Check the status of Carrier1/1 with the command

   ``# aviatcli carrier show status Carrier1/1``


|   Configure radio with the configuration: (a) bandwidth 1000MHz, (b)
  transmit frequency 83500000 KHz, (c) receive frequency 73500000
  KHz, (d) modulation of QAM-16, and (e) transmission power of 14.5 dBm.


    ``# aviatcli carrier configure all -b 1000 -t 83500000 -r 73500000 -f qam-16 -p 14.5 mmwCarrier1/1``

    Example output ::

      root@demohost:~# aviatcli carrier configure all -b 1000 -t 81500000 -r 71500000 -f qam-16 -p 14.5 mmwCarrier1/1

      Welcome to the WTM4800 Multi-band CLI

      admin connected from 10.188.0.131 using ssh on WilsonHall
      WilsonHall# config
      Entering configuration mode terminal
      WilsonHall(config)# interface mmwCarrier1/1
      WilsonHall(config-interface-mmwCarrier1/1)# power rtpc selected-output-power 14.5
      WilsonHall(config-interface-mmwCarrier1/1)# bandwidth ANSI 1000.0
      WilsonHall(config-interface-mmwCarrier1/1)# tx-frequency 83500000
      WilsonHall(config-interface-mmwCarrier1/1)# rx-frequency 73500000
      WilsonHall(config-interface-mmwCarrier1/1)# coding-modulation Fixed qam-16
      WilsonHall(config-interface-mmwCarrier1/1)# commit

      Welcome to the WTM4800 Multi-band CLI

      admin connected from 10.188.2.4 using ssh on AgFarm
      AgFarm# config
      Entering configuration mode terminal
      AgFarm(config)# interface mmwCarrier1/1
      AgFarm(config-interface-mmwCarrier1/1)# power rtpc selected-output-power 14.5
      AgFarm(config-interface-mmwCarrier1/1)# bandwidth ANSI 1000.0
      AgFarm(config-interface-mmwCarrier1/1)# tx-frequency 73500000
      AgFarm(config-interface-mmwCarrier1/1)# rx-frequency 83500000
      AgFarm(config-interface-mmwCarrier1/1)# coding-modulation Fixed qam-16
      AgFarm(config-interface-mmwCarrier1/1)# commit
|
