.. _AraOptical_Experiment_Link_Status:

AraOptical (FSOC) Link Status Check 
-------------------------------------------------

**Resources needed:** AraOptical link and AraHaul host servers.

**Short Description:** The experiment allows users to check the status
of the ARA Free Space Optical (FSO) link using related metrics.

.. note:: For this experiment, the FSOC link does not need to be
	  up. In other words, this experiment can be performed even if
	  the link appears as **unavailable** on the resource overview
	  page.

Figure below illustrates the experiment setup, involving a long-range
(~6.5 miles) FSO AraHaul link between Wilson Hall and Agronomy Farm.

.. figure:: images/AraOp_Exp1.png
   :align: center
|

**Detailed Steps for the Experiment**

Follow the steps below to create leases and launch containers for this
experiment:

#. Login to the portal `<portal.arawireless.org>`_  with login
   credentials.

#. Create two reservations using the *Project -> Reservations ->
   Leases* tab from the dashboard.

      1. Aviat Host Computer at Wilson Hall

	 * *Site*: Wilson Hall  
	 * *Resource Type*: AraHaul  
	 * *Device Type*: Host
	 * *Device ID*: 000

      2. Aviat Host Computer at Agronomy Farm

	 * *Site*: Agronomy Farm
	 * *Resource Type*: AraHaul
	 * *Device Type*: Host
	 * *Device ID*: 000

#. Launch containers, using *Project -> Containers* tab in the
   dashboard, on both reserved nodes using the corresponding
   reservation IDs. For containers, use the following attributes:

   * *Container Image:*  ``arawirelesshub/araopticalcli:ssh``
   * *Network:* ARA_Shared_Net. 

   Remaining attributes can be provided at your discretion. Refer to
   :ref:`ARA Hello World Experiment <ARA_Hello_World>` for default
   settings.

#. Once the container is launched, note the floating IPs to access the
   containers remotely through SSH via ARA jumpbox. The containers can
   be accessed via the **Console** tab of the respective containers in
   the *Project -> Containers* tab from the dashboard or using SSH via
   the jumpbox server.  Visit :ref:`ARA_Jumpbox` for more information
   on accessing containers via jumpbox.


#. For retrieving the **received signal power** (both in dBm and mW)
   from the FSO link, execute the following commands in the respective
   container:

   * For rx power output in **mW** and **dBm**::

       # araopticalcli interface status show xe-0/0/4 -f rx-signal-avg-optical-power

     Example output from Wilson Hall container::

       root@WilsonHall-Host-000:/# araopticalcli interface status show xe-0/0/4 -f rx-signal-avg-optical-power-dbm
       Interface: xe-0/0/4
       rx-signal-avg-optical-power-dbm -33.01
       root@WilsonHall-Host-000:/# araopticalcli interface status show xe-0/0/4 -f rx-signal-avg-optical-power
       Interface: xe-0/0/4
       rx-signal-avg-optical-power     0.0005
       rx-signal-avg-optical-power-dbm -33.01

     
   .. note:: Here, the received power is measured from the **optical
	     switch**.

   To check the **received signal power** in remote mode, i.e.,
   retrieve the received power at Wilson Hall FSOC from the Agronomy
   Farm or vice versa, use the following command in the respective
   container console:

   * For rx power output in **mW** and **dBm**::

       # araopticalcli interface status show xe-0/0/4 -f rx-signal-avg-optical-power --remote

     Example output: ::

        root@WilsonHall-Host-000:/# araopticalcli interface status show xe-0/0/4 -f rx-signal-avg-optical-power-dbm --remote
        Interface: xe-0/0/4
        rx-signal-avg-optical-power-dbm -40.00
        root@WilsonHall-Host-000:/# araopticalcli interface status show xe-0/0/4 -f rx-signal-avg-optical-power --remote
        Interface: xe-0/0/4
        rx-signal-avg-optical-power     0.0001
        rx-signal-avg-optical-power-dbm -40.00
    
   To check the whole **laser output**, i.e., the transmit power of
   the FSOC link **measured at the switch**, the following commands
   can be used:

   * For laser output in **mW** and **dBm**::

       # araopticalcli interface status show xe-0/0/4 -f laser-output-power
     
     Example output ::

        root@WilsonHall-Host-000:/# araopticalcli interface status show xe-0/0/4 -f laser-output-power-dbm
        Interface: xe-0/0/4
        laser-output-power-dbm  2.01
        root@WilsonHall-Host-000:/# araopticalcli interface status show xe-0/0/4 -f laser-output-power
        Interface: xe-0/0/4
        laser-output-power      1.5870
        laser-output-power-dbm  2.01
        root@WilsonHall-Host-000:/#

   We are working on additional metrics available from the switch, and
   will be made available for researchers soon. 
   
   ..
      To check all the available **interface metric**, we can use the
      command below. ::

	# araopticalcli interface metric ls

      Example output ::

	   root@WilsonHall-Host-000:/# araopticalcli interface metric ls
	   laser-rx-power-low-alarm-threshold  (unit: mW)
	   laser-bias-current-high-alarm (unit: mA / milliamps)
	   laser-rx-power-high-warn-threshold-dbm (unit: dBm)
	   laser-rx-power-low-alarm-threshold-dbm (unit: dBm)
	   laser-rx-power-high-warn-threshold (unit: mW)
	   laser-tx-power-high-warn-threshold (unit: mW)
	   module-temperature-high-alarm-threshold (unit: degrees C / degrees F)
	   module-voltage-high-alarm (unit: Volts)
	   laser-rx-power-low-warn (unit: mW)
	   module-voltage-high-alarm-threshold (unit: Volts)
	   laser-rx-power-high-alarm-threshold-dbm (unit: dBm)
	   laser-rx-power-high-warn (unit: mW)
	   module-temperature-low-alarm (unit: degrees C / degrees F)
	   laser-rx-power-low-alarm (unit: mW)
	   module-temperature (unit: degrees C / degrees F)
	   module-temperature-low-alarm-threshold (unit: degrees C / degrees F)
	   module-temperature-high-warn (unit: degrees C / degrees F)
	   laser-rx-power-low-warn-threshold (unit: mW)
	   laser-rx-power-high-alarm (unit: mW)
	   module-voltage (unit: Volts)
	   laser-bias-current (unit: mA / milliamps)
	   laser-tx-power-high-alarm-threshold (unit: mW)
	   module-temperature-high-warn-threshold (unit: degrees C / degrees F)
	   module-voltage-high-warn-threshold (unit: Volts)
	   laser-bias-current-high-warn-threshold (unit: mA / milliamps)
	   module-temperature-low-warn (unit: degrees C / degrees F)
	   module-voltage-low-alarm (unit: Volts)
	   laser-tx-power-low-alarm-threshold-dbm (unit: dBm)
	   laser-tx-power-low-warn-threshold-dbm (unit: dBm)
	   laser-bias-current-low-warn (unit: mA / milliamps)
	   laser-bias-current-high-alarm-threshold (unit: mA / milliamps)
	   module-temperature-low-warn-threshold (unit: degrees C / degrees F)
	   laser-bias-current-low-alarm (unit: mA / milliamps)
	   laser-bias-current-high-warn (unit: mA / milliamps)
	   module-voltage-low-warn (unit: Volts)
	   laser-rx-power-low-warn-threshold-dbm (unit: dBm)
	   laser-tx-power-low-warn-threshold (unit: mW)
	   laser-output-power-dbm (unit: dBm)
	   module-voltage-low-alarm-threshold (unit: Volts)
	   rx-signal-avg-optical-power (unit: mW)
	   module-voltage-low-warn-threshold (unit: Volts)
	   laser-rx-power-high-alarm-threshold (unit: mW)
	   laser-tx-power-low-alarm-threshold (unit: mW)
	   laser-output-power (unit: mW)
	   laser-tx-power-low-warn (unit: mW)
	   module-voltage-high-warn (unit: Volts)
	   laser-bias-current-low-alarm-threshold (unit: mA / milliamps)
	   laser-bias-current-low-warn-threshold (unit: mA / milliamps)
	   laser-tx-power-high-alarm-threshold-dbm (unit: dBm)
	   laser-tx-power-low-alarm (unit: mW)
	   laser-tx-power-high-alarm (unit: mW)
	   rx-signal-avg-optical-power-dbm (unit: dBm)
	   laser-tx-power-high-warn (unit: mW)
	   laser-tx-power-high-warn-threshold-dbm (unit: dBm)
	   module-temperature-high-alarm (unit: degrees C / degrees F)

      To view the output of all available parameters for an FSOC switch interface simultaneously, the following command can be used.
      Additionally, to check the output of a specific paramenter, simply the filter flag ``-f`` can be used in the 
      command line below, followed by the **interface metric** name from the above list. 

	``# araopticalcli interface status show xe-0/0/4``

	Example output: ::

	   root@WilsonHall-Host-000:/# araopticalcli interface status show xe-0/0/4
	   Interface: xe-0/0/4
	   laser-bias-current      72.240
	   laser-output-power      1.5870
	   laser-output-power-dbm  2.01
	   module-temperature      45 degrees C / 113 degrees F
	   module-voltage  3.2250
	   rx-signal-avg-optical-power     0.0005
	   rx-signal-avg-optical-power-dbm -33.01
	   laser-bias-current-high-alarm   off
	   laser-bias-current-low-alarm    off
	   laser-bias-current-high-warn    off
	   laser-bias-current-low-warn     off
	   laser-tx-power-high-alarm       off
	   laser-tx-power-low-alarm        off
	   laser-tx-power-high-warn        off
	   laser-tx-power-low-warn off
	   module-temperature-high-alarm   off
	   module-temperature-low-alarm    off
	   module-temperature-high-warn    off
	   module-temperature-low-warn     off
	   module-voltage-high-alarm       off
	   module-voltage-low-alarm        off
	   module-voltage-high-warn        off
	   module-voltage-low-warn off
	   laser-rx-power-high-alarm       off
	   laser-rx-power-low-alarm        on
	   laser-rx-power-high-warn        off
	   laser-rx-power-low-warn off
	   laser-bias-current-high-alarm-threshold 120.000
	   laser-bias-current-low-alarm-threshold  10.000
	   laser-bias-current-high-warn-threshold  110.000
	   laser-bias-current-low-warn-threshold   20.000
	   laser-tx-power-high-alarm-threshold     3.1620
	   laser-tx-power-high-alarm-threshold-dbm 5.00
	   laser-tx-power-low-alarm-threshold      0.5010
	   laser-tx-power-low-alarm-threshold-dbm  -3.00
	   laser-tx-power-high-warn-threshold      2.5110
	   laser-tx-power-high-warn-threshold-dbm  4.00
	   laser-tx-power-low-warn-threshold       0.6310
	   laser-tx-power-low-warn-threshold-dbm   -2.00
	   module-temperature-high-alarm-threshold 88 degrees C / 190 degrees F
	   module-temperature-low-alarm-threshold  -43 degrees C / -45 degrees F
	   module-temperature-high-warn-threshold  85 degrees C / 185 degrees F
	   module-temperature-low-warn-threshold   -40 degrees C / -40 degrees F
	   module-voltage-high-alarm-threshold     3.600
	   module-voltage-low-alarm-threshold      3.000
	   module-voltage-high-warn-threshold      3.500
	   module-voltage-low-warn-threshold       3.100
	   laser-rx-power-high-alarm-threshold     0.1996
	   laser-rx-power-high-alarm-threshold-dbm -7.00
	   laser-rx-power-low-alarm-threshold      0.0026
	   laser-rx-power-low-alarm-threshold-dbm  -25.85
	   laser-rx-power-high-warn-threshold      0.1260
	   laser-rx-power-high-warn-threshold-dbm  -9.00
	   laser-rx-power-low-warn-threshold       0.0040
	   laser-rx-power-low-warn-threshold-dbm   -23.98     

   
