.. _AraRAN_Experiments:

AraRAN Experiments
==============================

* **Open-Source NextG**

  * :ref:`AraRAN_Experiment_Gnuradio`
  * :ref:`AraRAN_Experiment_OAI_Indoor`
  * :ref:`AraRAN_Experiment_OAI_Outdoor`
  * :ref:`AraRAN_srsRAN_Indoor`

* **O-RAN**

  * :ref:`AraRAN_Experiments_ORAN`

* **TVWS Massive MIMO**

  * :ref:`AraRAN_Experiment_Skylark_Link_Behavior`
  * :ref:`AraRAN_Experiment_Skylark_CSI`
  * :ref:`AraRAN_Experiment_Skylark_UE_Measurement`
  ..
     * :ref:`AraRAN_Experiment_Skylark_Modding`

* **COTS 5G**

  * :ref:`AraRAN_Experiment_COTS_Measurement`
  
* **Spectrum Innovation**

  * :ref:`AraRAN_Experiment_Spectrum_Sensing`




