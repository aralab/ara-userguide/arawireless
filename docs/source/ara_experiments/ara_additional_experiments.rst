.. _ARA_Additional_Experiments:

Additional Experiments
=============================

The experiments here are designed to use ARA resources, other than
AraRAN and AraHaul, such as weather sensors, disdrometers, and edge
compute nodes. 

     * :ref:`ARA_Weather_Experiment`
     * :ref:`ARA_Edge_Experiment`
