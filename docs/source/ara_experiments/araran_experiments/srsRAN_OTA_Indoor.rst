.. _AraRAN_srsRAN_Indoor:   

Basic srsRAN Experiment in Sandbox using USRPs
-----------------------------------------------------------------------------

**Platform:** srsRAN and Universal Software Radio Peripherals

**Resources used:** USRP B210 (BS and UE), NUC13 Pro computer, Panaroma Antennas, USB 3.0 cables

..
   **Short Description:** The experiment demonstrates a basic
   over-the-air 4G network using srsRAN_4G and USRPs. The setup
   includes an srsepc, srsenb, and srsue. 
   

**Description:** This experiment features a 4G network deployment
using containerized instances of the major components of srsRAN, i.e.,
a containerized evolved packet core (``srsepc``), containerized eNB
(``srsenb``), and a containerized UE (``srsue``), deployed on Intel
x86 servers. The following figure shows the 5G network created from
the experiment.

   .. note :: srsRAN currently supports only 15kHz sub-carrier spacing
              on srsue.  Therefore, any experiment using SDR-based
              srsue is limited to 4G Frequency Division Duplex (FDD).
      

.. image:: images/srsRAN_Sandbox_4G.png
   :align: center
| 

**Detailed Steps for the Experiment**

#. Login to `ARA portal <https://portal.arawireless.org>`_ with your
   credentials.

#. Create two reservations using the *Project -> Reservations ->
   Leases* tab from the dashboard.

   1. eNB

      * *Site*: Sandbox  
      * *Resource Type*: AraRAN  
      * *Device Type*: Host
      * *Device ID*: 005

   2. UE

      * *Site*: Sandbox
      * *Resource Type*: AraRAN
      * *Device Type*: Host
      * *Device ID*: 001


#. Create the following two containers on the respective nodes. For
   the containers, the Docker images can be used as follows:


   1. eNB

      * *Container Image*: ``arawirelesshub/srsran:srsenb``
      * *CPU*: 6
      * *Memory*: 8192
      * *Network*: ARA_Shared_Net

   2. UE

      * *Container Image*: ``arawirelesshub/srsran:srsue_4g``
      * *CPU*: 6
      * *Memory*: 8192
      * *Network*: ARA_Shared_Net


#. The containers can be accessed via the console tab of the
   respective containers in the *Project -> Containers* menu from the
   dashboard, or via SSH through the jumpbox server using the floating
   IP. For this experiment, we recommend to use the jumpbox approach
   since we need multiple terminal access to the same container. Visit
   :ref:`ARA_Jumpbox` for more information on accessing containers via
   jumpbox.

#. In both containers, run the following command to check the radios
   connected to the host. In case the command cannot find SDR, refer
   the instructions in :ref:`FAQ <FAQ_Permission_Denied_SDR>`. ::

	# uhd_find_devices
	
   The output of the above command should look like the following image.

   .. image:: images/UHD_Find_Devices_6.png
      :align: center
      
#. Open **three** different terminals for the base station. The first
   one to run the evolved packet core (EPC), the second is to run the
   eNB instance, and the third to perform the delay (*ping*) and
   throughput experiments (*iperf*).
   
#. For the UE, open **two** separate terminals. The first one is to
   run the UE instance and the other for delay (*ping*) and throughput
   experiments (*iperf*).
   
#. In the first terminal of the base station, run the following
   command to start the EPC. ::

     # srsepc

   The command generates the output as below.
    
   .. image:: images/srsepc.png
      :align: center

#. In the second terminal of the base station, run the following
   command to start eNB. ::

     # srsenb

   The output of the above command will be as shown in the following
   figure. 

   .. image:: images/srsenb.png
      :align: center

#. In the first terminal of the user equipment, run the UE using the
   following command. ::

     # srsue

   The command ``srsue`` generates the output as provided below.

   .. image:: images/srsue.png
      :align: center
      

#. **Ping test to the Core Network**: On the second UE terminal, run
   the following command to ping the EPC. ::

     # ping 172.16.0.1

   An example output of the *ping* command is shown below.

     .. image:: images/ueping.png
	:align: center

   **Console Traces**

   On establishing a successful connection, press **t** followed by
   **ENTER** on the ``srsenb`` and ``srsue`` terminals to observe the
   link metrics as seen below.
   
   	**eNB Console Trace**
	
	.. image:: images/srsenb_console.png
           :align: center
	| 

	**UE Console Trace**
	
	.. image:: images/srsue_console.png
           :align: center
           
#. **End-to-end throughput test**: For measuring the end-to-end
   throughput (from UE to the EPC), we run the *iperf* server at the
   EPC and *iperf* client at the UE.

   For the *iperf* server, execute the following command in the third
   terminal of the base station. ::

     # iperf -s -u -i 1
	
   On the second UE terminal, where you run the *ping* earlier, run
   the *iperf* client using the following command to perform the
   throughput test for 10 seconds. ::
   
     # iperf -c 172.16.0.1 -u -i 1 -b 25M -t 10
   
   The sample output of the *iperf server* is shown below.

     .. image:: images/iperfserver.png
	:align: center
   
   You can observe the **brate** section for both the console traces
   of the ``srsenb`` and ``srsue`` showing the actual throughput
   measured on the link.
   
   The console traces for the ``srsenb`` start as shown in the figure
   below.
   
   .. image:: images/brate.png
	:align: center
   
