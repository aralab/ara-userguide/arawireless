.. _AraRAN_Experiments_ORAN:

Field-Deployed OAI 5G RAN with Integrated E2 Agent and nearRT-RIC using O-RAN Compliant FlexRIC
----------------------------------------------------------------------------------------------------------------------------

**Platform:** OpenAirInterface5g, FlexRIC, and Universal Software Radio Peripherals.

**Resources used:** USRP N320 and Dell R750 server as gNB, USRP B210
and Supermicro server as nrUE, CommScope SS-65M-R2 antenna, Laird
OC69421 UE Antenna, Power Amplifiers, and Low Noise Amplifiers.


**Description:** This experiment features a 5G NR Radio Access Network
(RAN) deployment using containerized instances of OAI 5G softmodems
with O-RAN support. The experiment describes the steps of deployment
OAI 5G RAN with integrated E2 agent and a nearRT-RIC using O-RAN
compliant FlexRIC. The figure below shows the network diagram of the
experiment. 

.. image:: images/oran_experiment.png
   :align: center
|

**Detailed Steps for the Experiment**

#. Login to `ARA portal <https://portal.arawireless.org>`_ with your
   credentials.

#. Create two reservations using the *Project -> Reservations ->
   Leases* tab from the dashboard.

   1. gNB

      * *Site*: Curtiss Farm
      * *Resource Type*: AraRAN  
      * *Device Type*: Base Station
      * *Device ID*: 000

   2. UE

      * *Site*: Curtiss Farm
      * *Resource Type*: AraRAN
      * *Device Type*: User Equipment
      * *Device ID*: 001 or 002


#. Create the following two containers on the respective nodes using
   the corresponding reservation IDs. For the containers, use the
   following setting:

   1. gNB

      * *Container Image*: ``arawirelesshub/openairinterface5g:oran``
      * *CPU*: 8
      * *Memory*: 8192
      * *Network*: ARA_Shared_Net

   2. nrUE

      * *Container Image*: ``arawirelesshub/openairinterface5g:oai_nrue_outdoor``
      * *CPU*: 8
      * *Memory*: 8192
      * *Network*: ARA_Shared_Net

#. Once the container is launched, take a note on the floating IP if
   you want to access the container from your PC via ARA jumpbox. The
   containers can be accessed via the console tab of the respective
   containers in the *Project -> Containers* tab from the dashboard or
   using SSH via the ARA jumpbox server. Visit :ref:`ARA_Jumpbox`
   for more information on accessing containers via jumpbox. 

#. In both containers, run the following command to check the radios
   connected to the host. ::

	# uhd_find_devices
	
   The output of the above command should look like the following image.

   .. image:: images/uhd_find_curtiss.png
      :align: center

#. In order to connect gNB to the core network, we need to provide the
   container's IP address and interface in the gNB configuration
   file. For finding the IP address of the container, run the
   following command.::

     # ifconfig

   Note down the interface with IP address ``192.168.70.113``. For
   example, for Curtiss Farm, the interface is ``eth4``.

#. Edit the configuration file to provide the Ethernet interface and
   IP address of the container.

   Open the configuration file. ::

        # nano ~/oai/targets/PROJECTS/GENERIC-NR-5GC/CONF/gnb.sa.band78.fr1.106PRB.usrpb210.conf

   To make gNB connected to our core network, we need to attach the
   gNB to the **AMF** of the core network. Edit the lines as shown in
   the figure below with the IP address and the interface
   corresponding to the IP address mentioned in Step 6.

   ..
      the figure below. Note that in the following image, we assume the
      IP address of the interface ``eth2`` as ``192.168.70.113``.

   .. image:: images/net_int.png
      :align: center

   Once the modification is complete, save and exit the nano editor.

#. Add a route to the core network from the gNB container with the
   following command. Please note that we need to provide the interface
   we identified from Step 6. (For Curtiss Farm, the interface
   provided at the end of the command should be ``eth4``). ::
	
	# ip route add 192.168.70.128/26 via 192.168.70.126 dev eth4


#. **Running the gNodeB:** In one terminal of the gNB container created, run the OAI gNB
   using the following commands. In case of any error, refer our 
   :ref:`FAQ <FAQ_Permission_Denied_SDR>`. ::

   	# cd ~/oai
   	# source oaienv
   	# cd cmake_targets/ran_build/build
   	# ./nr-softmodem -O ../../../targets/PROJECTS/GENERIC-NR-5GC/CONF/gnb.sa.band78.fr1.106PRB.usrpb210.conf --gNBs.[0].min_rxtxtime 6 --sa --usrp-tx-thread-config 1


#. **Starting nrUE:** In the UE container, run the OAI nrUE using the
   following commands. ::


   	# cd ~/openairinterface5g
   	# source oaienv
   	# cd cmake_targets/ran_build/build
   	# ./nr-uesoftmodem -O ../../../targets/PROJECTS/GENERIC-NR-5GC/CONF/ue.conf -r 106 --numerology 1 --band 78 -C 3604800000 --ue-fo-compensation --sa -E --ue-txgain 0 --nokrnmod 1


   In case of error or UE not connecting to gNB, refer :ref:`FAQ
   <FAQ_UE_not_Connecting_to_gNB>`. ::


#. **Starting nearRT-RIC:** In a second terminal of the **gNB
   container**, start the nearRT-RIC using the following commands. ::

   	# cd flexric
   	# ./build/examples/ric/nearRT-RIC


   The console trace when the ``flexRIC`` starts is shown below:

   .. image:: images/flexRIC.png
      :align: center

#. **Starting KPM monitor xApp:** In a third terminal of the **gNB
   container**, start the KPM monitor xApp using the following
   commands. ::

   	# cd flexric
   	# ./build/examples/xApp/c/monitor/xapp_kpm_moni



   The console trace when the ``KPM monitor xApp`` starts is shown below.

   .. image:: images/KPM_xApp.png
      :align: center


#. **Starting GTP monitor xApp:** In a fourth terminal of the **gNB
   container**, start the (MAC + RLC + PDCP) monitor xApp using the
   following commands. ::

   	# cd flexric
   	# ./build/examples/xApp/c/monitor/xapp_gtp_moni


   The console trace when the ``GTP monitor xApp`` starts is shown below.

   .. image:: images/gtp_xApp.png
      :align: center


#. **Starting (MAC + RLC + PDCP) monitor xApp:** In a fifth terminal
   of the gNB container, start the (MAC + RLC + PDCP) monitor xApp
   using the following commands. ::

   	# cd flexric
   	# ./build/examples/xApp/c/monitor/xapp_mac_rlc_pdcp_moni


   The console trace when the ``MAC_RLC_PDCP monitor xApp`` starts is
   shown below.

   .. image:: images/pdcp_xApp.png
      :align: center


   **Console traces for gNB and nrUE on different events**.

	**gNB when flexRIC starts**
	
	.. image:: images/gnb_flexRIC.png
           :align: center

	**gNB when connection with nrUE is established**
	
	.. image:: images/gnb_UE_attaches.png
           :align: center

	**nrUE when connection with gNB is established**
	
	.. image:: images/nrUE_terminal.png
           :align: center
