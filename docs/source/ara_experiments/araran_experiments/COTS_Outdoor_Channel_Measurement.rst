.. _AraRAN_Experiment_COTS_Measurement:

Outdoor 5G Channel Measurement using COTS UEs
------------------------------------------------------------------------

**Platform:** ARA COTS platform

**Resources Needed:**
  * **Ericsson Base Station:** RP 6647 (base band), AIR 6419 (mid-band
    radio), AIR 5322 (mmWave radio) 
  * **COTS UE:** Quectel RG530 and computer hosting the Quectel radio. 

**Description:** The experiment is designed to measure the wireless
channel characteristics using COTS UE from an already established
Ericsson RAN. Please note that in this experiment, we do not establish
any link. However, we measure the characteristics of an existing
link. The experiment uses a container only at the UE with a script for
measuring the channel characteristics. The following figure shows the
Ericsson network used for the experiment.

.. image:: images/Experiment_8.png
   :align: center


**Detailed Steps**

#. Login to `ARA portal <https://portal.arawireless.org/>`_ with your
   username and password. 

   .. note:: If you are a first time user, it is highly recommended to
		run the :ref:`Hello World experiment
		<ARA_Hello_World>` experiment first to get
		familiarized with the interface and the portal.

#. Create a reservation using the *Project -> Reservations ->
   Leases* tab from the dashboard with the following attributes:

      * *Site*: Ames
      * *Resource Type*: AraRAN
      * *Device Type*: User Equipment
      * *Device ID*: 000

   .. note:: For this experiment, you **DO NOT** need to reserve any
	     Base Station node. The measurement is performed only at
	     the UE side.

   In general, you can use any UE which is equipped with COTS Quectel
   radio. The following table provides the list of UEs that support
   this experiment.

      .. list-table:: List of Supported UEs for COTS Measurement
	 :widths: 5 5 5 10
	 :header-rows: 1
	 :align: center
      
	 * - Site
	   - Resource Type
	   - Device Type
	   - Device ID
	 * - Ames
	   - AraRAN
	   - User Equipment
	   - 000 and 140
	 * - Curtiss Farm
	   - AraRAN
	   - User Equipment
	   - 000, 001, and 002
	 * - Agronomy Farm
	   - AraRAN
	   - User Equipment
	   - 000, 001, 010, 020, 031, and 035
	 * - Feed Mill
	   - AraRAN
	   - User Equipment
	   - 000 and 001


#. Launch a container equipped using the container image
   ``arawirelesshub/cots:baseline``. You can keep the other container
   attributes according to your choice.

#. Once the container is launched, take a note on the floating IP and
   SSH to the container via the ARA jumpbox. Detailed instructions for
   accessing the container via jumpbox can be found :ref:`here
   <ARA_Jumpbox>`. 

#. In the container, run the following commands to start the
   measurement script.

   .. code-block:: console

      cd
      ./measure_cots.py -t 5

   Here, ``5`` represents the time in seconds we want to execute the
   measurement script and the samples are collected every 1
   second. You can provide the experiment duration as required.

   The command produces the following output.

   .. image:: images/COTS_Output.png
      :width: 600
      :align: center

#. The command in Step 5 also generates an output file named
   ``cots_YYYY_MM_DD-HH:MM:SS.csv`` (in CSV format) with the
   measurement data including the timestamp of the sample, band,
   ARFCN, RSRP, SINR, RSRQ, Cell ID, and Cell Name.  You can see the
   content of the generated file using the following command. Note
   that the timestamp in the filename represents the time at which the
   measurement started.

   .. code-block:: console

      cat <filename>

   An example of the content of the file from the above command is
   shown below.

   .. image:: images/COTS_CSV_Data.png
      :align: center
      :width: 600

   .. note:: The last column in the CSV file represents the Cell Name,
             which takes the form **ResourceName-SectorNumber**. For
             example, **WilsonHall-BS-000-2** indicates the 2nd sector
             of the base station specified as WilsonHall-BS-000.
    
#. If you want to save output into a custom file, you can use the
   ``-o`` option as follows.

   .. code-block:: console
		   
      ./measure_cots.py -t 5 -o output.csv

   The above command will generate the file named *output.csv*. If you
   run the command multiple times using the same filename, the output
   will be appended to the same file. 

