.. _AraRAN_Experiment_Spectrum_Sensing:

Spectrum Sensing 
--------------------------------------------

**Platform:** Software Defined Radios USRP B210

**Resources needed:** User equipment with USRP B210. 

**Short Description:** Collect the IQ samples for analyzing which
spectrum bands are occupied in the span of interest.


**Detailed Description:** The experiment makes use of USRP Hardware
Driver (UHD) and a Python script to compute the spectrum occupancy at
regular intervals. The script logs the frequency components in a
*.csv* file which can be copied by the users for analysis and
visualization.

Follow the steps below to create leases and launch containers for this experiment:

1. Login to the `ARA portal <https://portal.arawireless.org/>`_.

2. Create a reservation using the *Project -> Reservations -> Leases*
   tab from the dashboard using the following attributes:

	 * *Site*: CurtissFarm or Ames
	 * *Resource Type*: AraRAN  
	 * *Device Type*: User Equipment      


3. Launch a container on the reserved node using the corresponding
   reservation IDs as described in the :ref:`Hello World experiment <ARA_Hello_World>`. For the container, use the following Docker image:

	 * *Container Image*: ``arawirelesshub/ara-sensing:v1``

   Note that the container is equipped with UHD and other required
   dependencies. 

4. [Optional step if the user wants to access the container via SSH]
   Once the container is launched, take a note on the floating IP from
   the container information. 

5. The containers can be accessed via the console tab of the
   respective containers in the *Project -> Containers* tab from the
   dashboard or using ssh via the jumpbox server. Visit
   :ref:`ARA_Jumpbox` for more information on accessing containers via
   jumpbox.

6. In the container, run the following commands to start the experiment ::

	# cd /root
	# python3 sensing-experiment.py 

   The command starts the experiment and logs the spectrum occupancy
   information in different files in the same folder.  The users can
   copy the files to their computer and plot using a Python script or
   MATLAB and check the occupied spectrum bands between 3400 and 3600
   MHz at that location.

