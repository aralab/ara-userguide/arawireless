.. _AraRAN_Experiment_Skylark_CSI:

Capturing CSI Data using the AraMIMO Deployment
--------------------------------------------------------------

**Platform:** Skylark Wireless

**Resources needed:** Central Unit (CU), Distributed Unit (DU), Radio
Unit (RU), and a Customer Premises Equipment (CPE)/User Equipment (UE).

**Short Description**: The experiment is designed to capture raw CSI data
using AraMIMO deployment.

**Detailed Description:** AraMIMO consists of multiple CPEs connected
to the Base Station (BS), a Skylark Wireless Faros V2 equipment. Using
this experiment, we launch a container equipped with APIs and AraMIMO
Repo.  For the network diagram, refer to :ref:`AraRAN_Experiment_Skylark_Link_Behavior`.

**Detailed Steps**

#. Follow the steps of :ref:`AraRAN_Experiment_Skylark_Link_Behavior` up to Step 8 until the
   container is launched and the user has SSH access to the container.

#. Go to the root home directory. ::

	# cd

#. Go to the ``aramimo`` directory. ::

	# cd aramimo

#. The ``aramimo`` directory has multiple scripts to collect the
   measurement data. Run the csi script to start capturing the CSI
   data. ::

	# ./csi_capture_script.py <time in seconds>

   .. admonition:: NOTE 
	
      The time can not be greater than 100 seconds. Each frame is 10ms, so 100s would capture CSI data of 10,000 frames.

#. As an example, enter the time as 10 seconds and execute the
   script. The script connects to the Skylark CU and start collecting
   the data.

#. Once the CSI capture is complete, the data will be saved to the
   ``/home/root/csi_data`` directory.  The files can be saved locally
   to user's computer via jumpbox for analysis. This script captures 3
   files, i.e., ``csi.h5``, ``csi_info.json``, and
   ``hardware.json`` in a zip file. The file ``csi.h5`` contains the actual CSI data
   while the other two files contain system-related data such as data
   rates, SNR, and system parameters.

#. To save the files to local PC, first copy it over to the jumpbox
   server using ``scp``. On the jumpbox server run the command. ::
	
	# scp -r root@<floating_ip_of_container>:/home/root/csi_data ~/

#. The data from jumbox server can be copied to you local computer
   using the user's ssh-key (assuming the data is saved user's home
   directory on jumpbox server). ::
	
	# scp -i <path_to_user_private_key> -r <username>@jbox.arawireless.org:~/csi_data .

#. The whole ``csi_data`` folder will be copied to the current
   directory of your host computer where the above command is run.

#. To explore the CSI data, we need to unzip the zip file. Go to the ``csi_data``
   folder and run the following to untar the files. ::

      cd csi_data
      unzip csi_563000000.0_24000000.0_03_24_2024_02_40_52.zip

#. Here is an example script to read and explore the contents of the 
   h5 file. ::

      import h5py
      csi_read = h5py.File('csi.h5', 'r')
      for key in csi_read.keys():
         print(key)
         for subkey in csi_read[key].keys():
            print(csi_read[key][subkey])

#. Keys are `ref_uplink`, `ref_downlink` and `ue_uplink`. Subkeys are `bin`, 
   `ch`, `data`, `frame`, `hdl`. 

#. The `hdl` (or handle) is the user number. `ch` (or channel) is the 
   channel of the `hdl` (each user has two channels, 0 and 1). `frame` is the
   frame number, `bin` is the frequency offset from the center frequency. 
   `data` contains the actual IQ values for each `hdl`, `ch` and `frame`.
    
#. As an example, following line will print the IQ data for the uplink 
   pilots in first `frame`, first `hdl`, and first `ch`.  ::

      print(list(csi_read['ue_uplink']['data'][0:512]))

#. The BS has a total of 42 antennas. The uplink pilot from each UE
   is received on all the antennas. The first 4 values in the 512 values
   above are for the reference antennas. There are two reference antennas,
   which also receive these pilots and the first 4 values (I and Q) in 
   the above 512 values correspond to these reference antennas. The next
   84 values are the I and Q values on the 42 BS antennas. The remaining
   values has no data becuase we only has 43 antennas, however the application
   support upto 256 antennas. 

#. The following will print the first 100 `hdl`, `ch`, `frame` and `bin`
   respectively. ::

      print(list(csi_read['ue_uplink']['hdl'][0:100]))
      print(list(csi_read['ue_uplink']['ch'][0:100]))
      print(list(csi_read['ue_uplink']['frame'][0:100]))
      print(list(csi_read['ue_uplink']['bin'][0:100]))

#. To check which `hdl` correspond to which UE, run the following command in 
   `ara-sklk-cli`. ::

      get_connected_ues
