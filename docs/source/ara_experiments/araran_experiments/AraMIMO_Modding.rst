.. _AraRAN_Experiment_Skylark_Modding:

Modding API Experiment on the AraMIMO TVWS Platform
------------------------------------------------------

**Platform:** Skylark Wireless

**Resources needed:** Central Unit (CU), Distributed Unit (DU), Radio
Unit (RU), and a Customer Premises Equipment (CPE)/User Equipment (UE).

**Short Description**: The experiment helps the user understand how to 
use the modding API on the AraMIMO deployment.

**Detailed Description:** AraMIMO consists of multiple CPEs connected
to the Base Station (BS), a Skylark Wireless Faros V2 equipment. Using
this experiment, we reserve the Skylark compute resource and actually login
to the Skylark CU. 

.. admonition:: NOTE 
   For this experiment the users need to inform ARA team initially since it 
   requires creating experiment user on the Skylark CU which can be used to 
   login to the CU. Only users who intend to use Modding API will be able to 
   access the Skylark CU to allow full programmability with some access 
   control policies in place. 

**Detailed Steps**

#. Follow the steps of :ref:`AraRAN_Experiment_Skylark_Link_Behavior` up to 
   Step 11 until the container is launched and the user has SSH access to 
   the container.

#. Upload the private key to the container. You can copy paste the key in the 
   container. ::

        mkdir .ssh
        touch .ssh/my_key
        chmod 600 .ssh/my_key
        nano .ssh/my_key

#. Paste the key in the `my_key` and save the file (`Ctrl+x` and then press `Enter`).
   The key will look something like ::

        -----BEGIN OPENSSH PRIVATE KEY-----

        ...
        <Long Key String>
        ...

        -----END OPENSSH PRIVATE KEY-----


#. Now ssh to the Skylark CU using the username created and the ssh key. 
   This will be same username and key that is used for jumpbox. ::

        ssh -i .ssh/my_key <username>@10.188.16.1

#. Your home directory should have three files initially i.e. `hw_config.yaml`
   which is BS configuration file, `restart_gyro_phy.py` which can be used for
   restarting the PHY application in case there are any issues and 
   `restart_mu2control.py` which can be used to restart the mu2control CLI 
   service. 

#. Clone the modding library repo. ::

        git clone https://github.com/skylarkwireless/sklk-phy-mod.git
        cd sklk-phy-mod

#. You can make desired changes or checkout your own fork or just proceed
   with it in its current basic reference design. 

#. Create build directory and compile the code. ::

        mkdir build_files
        cd build_files
        cmake ../ .
        make -j

#. A shared object file is created in `~/sklk-phy-mod/build_files/ref_design/`
   after the above step is successful. 

#. Edit the configuration file `hw_config.yaml` and enable modding i.e. uncomment
   the `mod_library` line under the `phy_config` and add the correct path and 
   save the file. Note that the correct path will look like 
   `/opt/<username>/sklk-phy-mod/build_files/ref_design/libsklkphy_mod_ref_design.so`. ::

        cd
        nano hw_config.yaml

#. Restart the gyro_phy and Mu2control to start with clean config. ::

        python3 restart_gyro_phy.py
        python3 restart_mu2control.py

#. Launch mu2control CLI. ::

        mu2control-cli 

#. Set the path for configuration file. ::

        set_config_file ./hw_config.yaml


#. Check whether the correct configuration is loaded and the mod_library 
   entry is added to the config file. ::

        get_config

#. Start the BS. ::

        config 

#. Monitor the log in another terminal. ::

        get_journal_entries gyro_phy 20 --json -w 1


#. Check if the UE gets connected and the stats. ::

        get_client_stats

#. Stop other UEs and try to connect only one UE to verify the working.

#. Type `help -v` to see all the commands available and full control of
   the application. 

    .. admonition:: NOTE 
        
        Right now the reference design is not fully functional and its just
        template so when the UE connects the gyro_phy application crashes. 
        The ref design can be modified to keep it stable. 


#. Exit the CU and the container, delete the container and lease after 
   the experiment is done. A script will be automatically run to 
   restore the BS to its defaults. 





