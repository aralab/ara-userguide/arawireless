.. _AraHaul_Experiments:

AraHaul Experiments
==================================

* **Wireless X-Haul**

  * :ref:`AraHaul_Experiment_Link_Configuration`
  * :ref:`AraHaul_Experiment_Link_Latency_Throuhput`

* **Free Space Optical (FSO) X-Haul**

  * :ref:`AraOptical_Experiment_Link_Status`
  * :ref:`AraOptical_Experiment_Link_Latency_Throuhput`

* **Satellite X-Haul**

  * :ref:`AraHaul_Experiment_Satcom_Access`
