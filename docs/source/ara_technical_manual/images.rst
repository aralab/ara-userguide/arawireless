Software Images Available in ARA
=============================================

For ARA users, we provide pre-built container images for
experimentation. The container images are equipped with software tools
specifically for the domain of experimentation. For instance, the
containers for RAN experiments are equipped with USRP Hardware Driver
(UHD) and OpenSource 5G stacks for Base Stations and User
Equipment. For AraHaul and other types of AraRAN experiments, we
create containers with wrapper APIs for measurement and
configuration. The following tables provide the description of
container images made available for ARA users.


Base Images
----------------

.. list-table:: 
   :widths: 15 35
   :header-rows: 1
   :align: left

   * - Image ID
     - Description
   * - ``arawirelesshub/ara-ubuntu:22.04``
     - Base Ubuntu 22.04 LTS image with SSH, Emacs, Vim, Nano, and
       network tools. 

AraMIMO Images
---------------

.. list-table:: 
   :widths: 15 35
   :header-rows: 1
   :align: left

   * - Image ID
     - Description
   * - ``arawirelesshub/aramimo:v1.1``
     - Container integrated with APIs for MIMO experiments.

AraRAN Images
---------------

.. list-table:: 
   :widths: 15 35
   :header-rows: 1
   :align: left

   * - Image ID
     - Description
   * - ``arawirelesshub/ara-sensing:v1``
     - The container provides wrapper APIs for spectrum sensing
       experiment.
   * - ``arawirelesshub/openairinterface5g:oai_gnb``
     - Container with openairinterface5g gNB for **indoor**
       experiments in **ARA Sandbox**.
   * - ``arawirelesshub/openairinterface5g:oai_nrue``
     - Container with openairinterface5g nrUE for **indoor**
       experiments in **ARA Sandbox**.
   * - ``arawirelesshub/openairinterface5g:oai_gnb_curtiss``
     - Container with OAI gNB for **outdoor** experiments at **Curtiss
       Farm**.
   * - ``arawirelesshub/openairinterface5g:oai_nrue_outdoor``
     - Container with OAI nrUE for **outdoor** experiments at
       **Curtiss Farm**.
   * - ``arawirelesshub/openairinterface5g:oai_gnb_agronomy``
     - Container with openairinterface5g nrUE for **outdoor**
       experiments at **Agronomy Farm**.
   * - ``arawirelesshub/openairinterface5g:ag_ue``
     - Container with OAI nrUE for **outdoor** experiments at
       **Agronomy Farm**.
   * - ``arawirelesshub/openairinterface5g:oai_gnb_wilson``
     - Container with openairinterface5g gNB for **outdoor**
       experiments at **Wilson Hall**.
   * - ``arawirelesshub/openairinterface5g:oai_nrue_wilson``
     - Container with openairinterface5g nrUE for **outdoor**
       experiments at **Wilson Hall**.
   * - ``arawirelesshub/srsran:srsenb``
     - Container with srsRAN eNB for **indoor** experiments at **ARA
       Sandbox**.
   * - ``arawirelesshub/srsran:srsue_4g``
     - Container with srsRAN UE for **indoor** experiments at **ARA
       Sandbox**.
   * - ``arawirelesshub/cots:baseline``
     - Container with scripts for RAN link measurements using COTS
       UEs.
      

AraHaul Images
------------------

.. list-table:: 
   :widths: 15 35
   :header-rows: 1
   :align: left

   * - Image ID
     - Description
   * - ``arawirelesshub/aviatcli:ssh``
     - Container with APIs for AraHaul (wireless backhaul) Aviat
       mmWave and microwave link experiments.


