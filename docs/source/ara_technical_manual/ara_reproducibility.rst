Experiment Reproducibility
#########################################

Experiment reproducibility is important concept in scientific method
where the experiment code and the data can be used to generate the
results. For achieving reproducibility, the experiment and data used
should be packaged and made available to the research community. We
enable experiment reproducibility using *experiment profiles*. 


Experiment Profile
==========================

For an experiment in ARA, a user reserves the resources required and,
further, launches containers on the reserved resources. Further, the
user accesses the containers via respective shell and executes the
experiment code. On reserving and launching containers, ARA portal
allows the user to download the specification of the experiment (i.e.,
the resources reserved and the containers along with related
attributes) as a YAML file, we call the *experiment profile*. Further,
the profile can be used to reproduce the experiment by uploading in
the portal. A major advantage of the profile that it automates the
process of resource reservation (i.e., creation of lease) and
instantiation of containers.

Creating and Experiment and Downloading the Experiment Profile
---------------------------------------------------------------------

#. Create an experiment following the :ref:`ARA experiment
   workflow<ARA_Experiment_Workflow>`, i.e., create required leases
   and launch containers on them. For this description, I use the
   our :ref:`Hello World! <ARA_Hello_World>` experiment, which
   requires a lease of a compute node (e.g., DataCenter-Compute-000)
   and a container launched on it. 

#. Download the experiment profile from the top-right corner of the
   ARA portal as shown in figure.

   .. image:: images/Experiment_Profile_Download.png
      :align: center

   The downloaded YAML file (say *demo_experiment.yaml*) may look as
   follows.

   .. code-block:: console

      heat_template_version: '2015-04-30'
      resources:
        lease_0:  
          type: OS::Blazar::Lease
          properties:
            start_date: ''
            end_date: ''
            name: Demo_Lease
            reservations:
	    - resource_properties:
	      - Site: DataCenter
		Resource_Type: ComputeNode
		Device_Type: Compute
		Device_ID: '000'
        container_0:
          type: OS::Zun::Container
          depends_on:
	  - lease_0
	  properties:
	    command: bash
	    image: arawirelesshub/ara-ubuntu:22.04
	    name: Demo_Container
	    hints:
	      host_name: DataCenter-Compute-000
	    networks:
	      - network: ARA_Shared_Net


   The YAML file consists of different attributes of reservation
   (leases) and the containers under the key ``resources``, i.e., the
   leases and containers are considered as resources in the experiment
   profile. 

   The key ``lease_0`` represents unique ID (**not the name**) of the
   resource reservation or lease, where the type is specified as
   ``type: OS::Blazar::Lease``. Further, you can see the parameters
   for creating a lease (from the portal) are listed such as
   ``start_date``, ``end_date``, ``name``, ``Site``,
   ``Resource_Type``, ``Device_Type``, and ``Device_ID``.

   Similar to the lease, ``container_0`` represents the unique ID of
   the container (*not the name*) followed by the description of
   container. The containers have the type
   ``OS::Zun::Container``. Further description includes the parameters
   provided during the container creation in the portal such as
   ``command``, ``image``, ``host_name`` (which is assigned by ARA),
   and ``networks``.

   .. note:: The experiment profile can be downloaded by user from the
	     portal and does not need to be created manually.


Launching the Experiment Profile
--------------------------------------

In ARA portal, the term *Stack* is used to indicate experiment
profile. Therefore, the words *stack*, *experiment profile*, and
*profile* are used synonymously.  For launching an experiment profile
use the following steps:

#. Click on the *Project -> Orchestration -> Stacks* menu from the ARA
   portal dashboard.

   .. image:: images/Experiment_Profile_Launch.png
      :align: center

#. Click on the **+ Launch Stack** button on the top-right (indicated
   within a red rectangle), which pops up the following form to upload
   the experiment profile, i.e., the downloaded YAML file.

   .. image:: images/Experiment_Profile_Select.png
      :align: center

   Here, you can keep the *Profile Source* as File and upload the YAML
   file in your computer by clicking the **Choose File** button. Up on
   selecting the file, click *Next* button to provide a name for the
   profile/stack as follows:

   .. image:: images/Experiment_Profile_Name.png
      :align: center

#. Click the **+ Launch Stack** button to launch the experiment using
   the profile. On launching the profile, you can see the profile is
   listed in the page with status as *Create in Progress*. 

   .. image:: images/Experiment_Profile_Create_in_Progress.png
      :align: center
   |

   Once the reservation and containers are launched, the status will
   change to *Create Complete*.
   
   .. image:: images/Experiment_Profile_Create_Complete.png
      :align: center

#. Now you can see that the leases specified in the profile is created
   and listed in the *Project -> Leases* menu in the dashboard as
   follows:

   .. image:: images/Experiment_Profile_Lease.png
      :align: center
   |
 
   Also, the container will be created and listed in the *Project ->
   Container -> Containers* menu.

   .. image:: images/Experiment_Profile_Container.png
      :align: center
   

Deleting a Profile
--------------------------

Once you finish the experiment, you can delete the profile by clicking
the *Delete Stack* option from **Check Stack** button corresponding to
the stack as shown in the figure below.

   .. image:: images/Experiment_Profile_Delete.png
      :align: center


   
   


   

   
