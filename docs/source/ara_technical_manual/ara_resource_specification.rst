.. _ARA_Resource_Specification:


ARA Resource Specification 
=================================

Resources in ARA are specified using the following three attributes:

1. **Site:** The location/site where the resource resides.
2. **Resource Type**: Represents the domain where the resource is used
   for. In ARA, the resources are classified into three types.

     a. *AraRAN*: Access network resources.
     b. *AraHaul*: Backhaul resources.
     c. *Compute*: Compute resources.

3. **Device Type:** Indicates the type of device under each resource
   type. For example, AraRAN devices include *Base Station* and *User
   Equipment*.
4. **Device ID:** A 3-digit number representing the specific ID of a
   node. 


We use the naming convention ``SITE-DEVICE-ID`` for ARA resources
where ``SITE`` represents the deployment site of the resource,
``DEVICE`` is the type of device, and ``ID`` is a 3-digit identifier
to uniquely identify the device at a site. The specifications of
currently deployed **fixed-location** resources are provided in the
following table. All resources deployed at fixed locations are
available for users to perform their experimentation.

.. list-table:: 
   :widths: 15 15 10 15 15
   :header-rows: 1
   :align: center

   * - Resource Name
     - Site
     - Resource Type
     - Device Type
     - Site Type 
   * - DataCenter-Compute-[000-001]
     - Data Center
     - Compute
     - Compute Node
     - Data Center
   * - AgronomyFarm-Edge-000
     - Agronomy Farm
     - Compute
     - Edge Node
     - Crop 
   * - ResearchPark-Edge-000
     - Research Park
     - Compute
     - Edge Node
     - Crop/Livestock
   * - WilsonHall-BS-000
     - Wilson Hall
     - AraRAN
     - Base Station
     - City
   * - WilsonHall-Host-000
     - Wilson Hall
     - AraHaul
     - Aviat
     - City
   * - AgronomyFarm-BS-000
     - Agronomy Farm
     - AraRAN
     - Base Station
     - Crop
   * - AgronomyFarm-Host-000
     - Agronomy Farm
     - AraHaul
     - Aviat
     - Crop
   * - CurtissFarm-BS-000
     - Curtiss Farm
     - AraRAN
     - Base Station
     - Crop
   * - ResearchPark-BS-000
     - Research Park
     - AraRAN
     - Base Station
     - Crop/Livestock
   * - Ames-UE-[000/140]
     - Ames
     - AraRAN
     - User Equipment
     - City 
   * - CurtissFarm-UE-[000-002]
     - Curtiss Farm
     - AraRAN
     - User Equipment
     - Crop
   * - KitchenFarm-UE-000
     - Kitchen Farm
     - AraRAN
     - User Equipment
     - Crop
   * - AgronomyFarm-UE-[000/001/010/020]
     - Agronomy Farm
     - AraRAN
     - User Equipment
     - Crop
   * - AgronomyFarm-UE-[031/032/033/035]
     - Agronomy Farm
     - AraRAN
     - User Equipment
     - Factory
   * - SheepFarm-UE-000
     - Sheep Farm
     - AraRAN
     - User Equipment
     - Livestock
   * - DairyFarm-UE-000
     - Dairy Farm
     - AraRAN
     - User Equipment
     - Livestock
   * - BeefFarm-UE-000
     - Beef Farm
     - AraRAN
     - User Equipment
     - Livestock
   * - FeedMill-UE-[000/001]
     - Beef Farm
     - AraRAN
     - User Equipment
     - Crop
   * - :ref:`Sandbox-Host-[001-025] <ARA_Sandbox_Service>`
     - Sandbox
     - AraRAN
     - Host
     - Laboratory

.. _ARA_Resource_Specification_Mobile_UEs:

**Mobile User Equipment**

ARA envisions to deploy mobile user equipment offering a platform for
experimenters to carry out their research in the domain of
communication involving mobility of nodes as well as for rural
application use cases and research. For realizing mobile experiments,
ARA deployed a couple of UEs in Phase-1, one on the bus operating as
part of the public transportation systems (shown using the red icon in
the deployment map below), and another on the vehicle involved in fire
and safety services. As discussed in :ref:`ARA_Infrastructure`, mobile
UEs include three wireless technologies including SDRs (B210s),
Skylark, and COTS (Quectel). At present, the mobile nodes are under
testing phase and will be made available to the researchers once they
are ready for reservation and use. The mobile UEs enable ARA users to
perform experiments to study link connectivity patterns during
mobility (i.e., the stability of links), behavior and modeling of
wireless channels, UE handovers, signal coverage from base stations,
and different modes of V2X communication, especially
vehicle-to-infrastructure. In addition, the dataset generated from the
experiments can be a valuable input for researchers in optimizing
future wireless communication systems.

Besides the UEs enabling remote experimentation for wireless
researchers, we deploy additional six mobile UEs (shown using blue
icons in the deployment map below), specifically to serve different
rural application use-cases. The six mobile nodes include: four on
phenobots (agriculture robots), one on the John Deere tractor, and
another on the Vermeer utility vehicle. The phenobots take
stereoscopic photographs of plants, helping agriculture scientists on
phenotyping process.  ARA provides a communication platform for
researchers to remotely control the phenobots as well as transferring
the data collected from phenobots to their cloud repository. A
demonstration of ARA-enabled phenobots can be found `here
<https://youtu.be/U98SoufPUiA?si=JD1221wiwDtbSX39>`_. Similar to
phenobots, the other two UEs serve the use-cases such as agricultural
vehicle tracking and video streaming from the farmlands. Besides
agriculture, ARA provides a communication platform for livestock farms
(such as dairy farm and sheep farm) to stream the real-time videos to
analyze the health and behavior of livestock. A demonstration of
ARA-enabled video streaming from sheep farm can be found `here
<https://youtu.be/ZqhRaEg0Fn8?si=W1_l83hnV-mTdt2F>`_.

A summary of the **mobile** UEs are provided in the following table.

.. list-table:: 
   :widths: 15 15 10 15 15
   :header-rows: 1
   :align: center

   * - Resource Name
     - Site
     - Resource Type
     - Device Type
     - Site Type 
   * - Ames-UE-120
     - Ames
     - AraRAN
     - User Equipment
     - Transportation (Application/Experimentation)
   * - Phenobot-UE-000
     - Agronomy Farm
     - AraRAN
     - User Equipment
     - Crop (Application)
   * - Phenobot-UE-001
     - Agronomy Farm
     - AraRAN
     - User Equipment
     - Crop (Application)
   * - Phenobot-UE-002
     - Curtiss Farm
     - AraRAN
     - User Equipment
     - Crop (Application)
   * - Phenobot-UE-003
     - Agronomy Farm
     - AraRAN
     - User Equipment
     - Crop (Application)
   * - DigitalAg-UE-000
     - Research Park
     - AraRAN
     - User Equipment
     - Crop (Application)
   * - DigitalAg-UE-001
     - Research Park
     - AraRAN
     - User Equipment
     - Crop (Application)
       

The spatial distribution of ARA is shown in the following map. The
yellow and green icons represent fixed BS and fixed UE sites,
respectively. The red and blue icons represents approximate location
of the mobile UEs.

.. raw:: html


         <iframe src="https://www.google.com/maps/d/embed?mid=1MmTVzSlxQxJk24oCBj8Y3n48tQUaprQ&ehbc=2E312F" width="1040" height="480"></iframe>	 

|

..
   The following table provides the description of each UE and the
   associated base station.

   .. list-table:: 
      :widths: 10 15 15 15
      :header-rows: 1
      :align: center

      * - User Equipment
	- SDR Base Station
	- Skylark Base Station
	- Ericsson Base Station
      * - AgronomyFarm-UE-000
	- Agronomy Farm [Available soon]
	- Wilson Hall [Available Soon]
	- Agronomy Farm [Available soon]
      * - AgronomyFarm-UE-001 
	- Agronomy Farm [Available soon]
	- Wilson Hall [Not available]
	- Agronomy Farm [Steady]
      * - AgronomyFarm-UE-010
	- Agronomy Farm [Available soon]
	- Wilson Hall [Not available]
	- Agronomy Farm [Steady]
      * - AgronomyFarm-UE-020
	- Agronomy Farm [Available soon]
	- Wilson Hall [Not available]
	- Agronomy Farm [Available soon]
      * - Ames-UE-000
	- Wilson Hall [Available]
	- Wilson Hall [Steady]
	- Wilson Hall [Steady]
      * - CurtissFarm-UE-000
	- Curtiss Farm [Available]
	- Wilson Hall [Available]
	- Curtiss Farm [Steady]
      * - CurtissFarm-UE-001
	- Curtiss Farm [Ready]
	- Wilson Hall [Ready]
	- Curtiss Farm [Steady]
      * - DairyFarm-UE-000
	- Research Park [Available]
	- Wilson Hall [Steady]
	- Research Park [Intermittent]
      * - KitchenFarm-UE-000
	- Agronomy Farm [Available]
	- Wilson Hall [Available soon]
	- Agronomy Farm [Intermittent]
      * - SheepFarm-UE-000
	- Research Park [Available]
	- Wilson Hall [Intermittent]
	- Research Park [Steady]


Besides the outdoor deployment, ARA provides a sandbox for
experimenters to test their experiments in a lab environment. Detailed
specification of the sandbox service can be found in the
:ref:`ARA_Sandbox_Service` page. 


ARA Resource Technical Specification
----------------------------------------

.. _AraRAN_Detailed_Spec:

AraRAN
^^^^^^^^^

As mentioned in :ref:`ARA Infrastructure <ARA_Infrastructure>`, AraRAN
is equipped with different wireless technologies including Software
Defined Radios, Massive MIMO, and COTS 5G solutions. Detailed
specification of different AraRAN components are provided below.

.. _SDR_Detailed_Spec:

Software Defined Radios
*****************************

 * **Base Station**: The SDR BSes are realized using NI USRP N320
   radios with operating frequency 3 MHz to 6 GHz with allowable
   instantaneous bandwidth of 200 MHz. The SDR supports standalone
   (embedded) or host-based (network streaming) operation. The master
   clock rates include 200, 245.76, and 250 MS/s. The SDR is equipped
   with Xilinx Zynq-7100 SoC Dual core ARM Cortex-A9\@800 MHz and
   builtin GPSDO.

   We use CommScope SS-65M-R2 4-port sector antenna  with average gain
   of 18 dBi (4300-3600MHz).

   The RF front-end operates on frequency band 3400-3600 MHz TDD with
   working bandwidth of 100 MHz. The maximum output power is 40 dBm
   and LNA gain of 25 dB. The automatic gain control is up to the
   given maximum output power.

.. _SDR_B210_Detailed_Spec:

 * **User Equipment**: The SDR UE is equipped with NI USRP B210, first
   fully integrated two channel USRP device with continuous RF
   coverage from 70 MHz - 6 GHz. It supports full duplex MIMO (2 Tx
   and 2 Rx) operation up to 56 MHz of real-time bandwidth (61.44 MS/s
   quadrature). The device supports GNU Radio and OpenBTS support
   through the open source USRP Hardware Driver (UHD). For advanced
   users, the SDR has open and reconfigurable Spartan 6 XC6SLX 150
   FPGA.

   We use 1 x Laird OC69421 direct mount omnidirectional antenna with
   gain of 5 dBi (3300-4200 MHz) and horizontal and vertical plane 3
   dB bandwidth of 360 degrees and 100 degrees, respectively.

   The RF front-end UE booster operates on the frequency band
   3400-3600 MHz TDD with working bandwidth 100 MHz. The maximum
   output power is 30 dBm and LNA gain of 12 dB. The automatic gain
   control is up to the given maximum output power.

.. _Skylark_Detailed_Spec:

Skylark
*********

 * **Base Station**: The base station consists of a Central Unit (CU),
   Distributed Unit (DU), and three Radio Units (RUs), one for each
   sector. Each RU has 7 radios and each radio is connected to a
   cross-polarized antenna having horizontal and vertical
   polarization. That is, each RU has 14x antennas per RU which makes
   a total of 42 antennas for three RUs at the BS covering 360 degrees
   cell area.  The DU can support up to 6 RUs, making total antenna
   count of 84. However, we deployed 3 RUs in Phase-1. The RUs are
   powered via a Power Distribution Unit (PDU) and are connected to
   the DU via FDU.

 * **Customer Premises Equipment (CPE)**: At present, six Skylark
   CPEs are deployed and 14 more CPEs are to be deployed in
   Phase-1. Each CPE has one radio that connects to a dual-polarized
   directional antenna. Omni directional antennas have been
   successfully tested with the CPE, however, the performance at long
   range is better for the directional antenna. Fixed UE nodes are
   equipped with directional antenna mounted on a pole while the
   mobile UEs use omni directional antenna.

 * **Frequency of Operation**: Skylark uses TV White Space (TVWS)
   band, i.e., 470–700 MHz, with target operating band 539- 593
   MHz. Both the BS and CPEs need to tune to same frequency in order
   to establish a connection. The bandwidth supported by the BS is 40
   MHz.


.. _AraHaul_Detailed_Spec:

AraHaul
^^^^^^^^^^^^^^^^

The wireless backhaul of ARA primarily consists of microwave and
millimeter wave links and free space optical link. 

.. _Aviat_Detailed_Spec:

Millimeter and Microwave Link
*******************************************

The long-range mmWave and microwave backhaul is realized using Aviat
WTM 4800 radios. Major features of the platform are as follows:

 * **Carriers**:
    1. E-Band (80 GHz)
      * Bandwidth (MHz): 3.75, 5, 10, 20, 25, 30, 40, 50, 60, 75, 80,
	and 100
      * Modulation: QPSK, 16QAM, 256QAM, 512QAM, 1024QAM, 2048QAM, and
	4096QAM
    2. Microwave (11 GHz)
      * Bandwidth (MHz): 250, 500, 750, 1000, 1500, and 2000
      * Modulation: QUARTER-QPSK, HALF-QPSK, QPSK, 16QAM, 256QAM,
	512QAM, 1024QAM, 2048QAM, and 4096QAM

 * **Capacity**: The dual transceiver design supports
    * Single channel E-band up to 10 Gbps
    * Dual channel multiband combining one E-band and one microwave
      channel aggregates up to 10 Gbps with or without adaptive dual
      carrier capacity.

 * **Power Control**: Fixed automatic transmitter power control (RTPC
   or ATPC)

.. _FSOC_Detailed_Spec:

Free Space Optical Link
***********************************

The long-range Free Space Optical Communication (FSOC) link between
base station sites operates with multiple channels in ITU grid at
C-band to realize up to 160 Gbps transmission capacity. The optical
backbone of the FSOC system consists of 16x10G dense
wavelength-division-multiplexing transceivers. Lasers are further
amplified by an Erbium-doped fiber amplifier. Major features of
optical link are as follows:

 * Operating wavelength: 1537nm-1564nm, ITU grid
 * Bandwidth: 5GHz-80GHz
 * Data rate: 10Gbps-160Gbps
 * Maximum transmitter output power: 30dBm
 * Receiver sensitivity: -23dBm
 * Fiber coupling loss: 6dB


ARA Radio Frequency Allocation
-------------------------------------------

The following table provides the spectrum used by each platform in
ARA.

.. list-table:: 
   :widths: 10 10
   :header-rows: 1
   :align: center

   * - Radio Platform
     - Frequency Band
   * - Software Defined Radio (SDR)
     - 3400 MHz - 3600 MHz
   * - Skylark/TVWS
     - 540 MHz - 570 MHz
   * - Ericsson
     - 3.45 GHz - 3.55 GHz, 27.5 GHz - 28.5 GHz
   * - Aviat
     - 11 GHz, 80 GHz


