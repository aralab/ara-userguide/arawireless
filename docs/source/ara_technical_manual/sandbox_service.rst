.. _ARA_Sandbox_Service:

ARA Sandbox Service
===================================================

Beside the outdoor deployment as specified in
:ref:`ARA_Resource_Specification`, ARA provides a sandbox for users to
test their experiments in a lab environment. The sandbox is equipped
with 50 USRP NI B210s in a room and deployed on a panel just below the
room ceiling (topology as shown in the figure below). Each SDR host
computer is connected to two SDRs via USB 3.0 cable, i.e., reserving
one node provide access to two SDRs. Further, the host computers are
connected to the ARA cloud via Netgear switches.

.. image:: images/ARA_Sandbox.png
   :align: center

|  

Sandbox Specification
----------------------------

* Software Defined Radio (SDR): USRP NI B210
* SDR Host Computer: Intel NUC 13

A snapshot of the sandbox is shown in the figure below.

.. image:: images/Sandbox_Snapshot.png
   :align: center

|


How to Use ARA Sandbox?
------------------------------

ARA considers sandbox as an ARA site similar to any other Base Station
(BS) or User Equipment (UE) location. Contrary to the other sites,
each SDR in the sandbox can be used either as a BS or as a
UE. Therefore, we define the device type of the host computer as
**Host**. The reservation and container instantiation in sandbox
follows the same procedures as any other resource. For example, to
create a lease, users can select the following attributes in the lease
form.

  * **Site:** Sandbox
  * **Resource Type:** AraRAN
  * **Device Type:** Host
  * **Device ID:** 001-025

On reserving the nodes, users can launch containers and execute the
experiments. 



