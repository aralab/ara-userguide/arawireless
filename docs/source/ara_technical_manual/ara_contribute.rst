.. _ARA_Contribute:

Contribute to the ARA Community
===================================

ARA aims to create a community of users, developers, and stakeholders
to foster collaboration in advancing existing and next generation
wireless communication systems. ARA offers a GitLab-based platform, as
part of the `Open-Source Ecosystem for Broadband Prairie (OPERA)
project <https://wici.iastate.edu/opera/>`_, ARA offers a GitLab-based
platform for sharing your experimentation experiences, including code,
experiment profiles, and documentation. You can access the OPERA
collaboration platform `https://gitlab.com/aralab/opera
<https://gitlab.com/aralab/opera>`_. The page hosts various project
groups such as OPERA Backbone, OPERA Collaboration, and OPERA
Learning.

* `OPERA Backbone <https://gitlab.com/aralab/opera/opera-backbone>`_:
  The group contains projects (or repositories) for generic code
  commits as well as those for specific wireless platforms such as
  AraRAN and AraHaul.

* `OPERA Collaboration
  <https://gitlab.com/aralab/opera/opera-collaboration>`_: The group
  focuses on creating and maintaining action clusters around ARA on
  specific domains. For example, the `ARA_AIML
  <https://gitlab.com/aralab/opera/opera-collaboration/ara_aiml>`_
  project can be used for establishing an AI/ML action cluster on
  ARA. If you are interested in introducing a new action cluster,
  please email us at e2@arawireless.org.

* `OPERA Learning <https://gitlab.com/aralab/opera/opera-learning>`_:
  The group is dedicated for documentation efforts. For example, if
  you would like to contribute to any documentation related to ARA,
  e.g., experimentation manuals, description of new experiments, etc.,
  you can use the `ARA Documentation
  <https://gitlab.com/aralab/opera/opera-learning/ara-documentation>`_
  project.

.. note:: The above-mentioned project groups are not limited to ARA. 
	  You can contribute to any topic of interest to the wireless
	  community.

Contribution Workflow
---------------------------

You can use Git-level operations to fork the projects to your account,
modify and commit the changes, and send a merge request to the remote
OPERA repository. The workflow for contributing to ARA is illustrated
in the following figure. Procedures performed by the user are shown in
the blue boxes, while those handled by the ARA maintainer are shown in
the green box.

.. image:: images/ARA_Contribution_WorkFlow.png
   :align: center


#. Login to your GitLab account.

#. Go to the OPERA project you want to contribute. For example, for
   this page, we use the `ARA Generic Notebooks
   <https://gitlab.com/aralab/opera/opera-backbone/ara-generic-notebooks>`_
   project. 

#. Now click on the fork button, marked with red rectangle in the
   figure below. 

   .. image:: images/Fork_Project.png
      :width: 800
      :align: center

   Now the OPERA project will be imported to your account.

#. Create a new branch for making your modification. Go to the
   imported project and click on the **+** button and **New Branch**
   as shown in the following figure. 

   .. image:: images/Create_New_Branch.png
      :width: 600
      :align: center

   Further, provide your branch name and source branch. The primary
   branch for any OPERA repository is named as **master**.

   .. image:: images/Branch_Name.png
      :width: 500
      :align: center

   .. note:: You can clone your repository to your local machine,
	     create a new branch, and later commit the modifications
	     (including the new branch) to your repository. 
   
#. Now, you can make the modifications (editing the existing files or
   adding/deleting files), save, and commit to your branch.

#. Once the commit is complete, send a merge request to OPERA source
   repository.

   * Select the **Merge requests** menu from the dashboard.

   * Click on the **New merge request** button.

     * | **Source branch:** Select the branch with your modifications. 
       | **Target branch:** Select the OPERA project repository and the branch.

       Further, click on the **Compare branches and continue** button,
       which directs you to a page where you can provide the following
       information.

     * **Title**: Provide a title for your merge request.

     * **Description:** A brief description on your modification.

   * Click on the **Create merge request** button at the bottom, which
     will send a notification to the OPERA project, where the
     maintainers can verify and merge your modifications to the target
     branch. 

If you have any questions, feel free to reach out at
support@arawireless.org. 
   



