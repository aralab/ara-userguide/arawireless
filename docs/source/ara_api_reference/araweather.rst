.. _ARA_Weather_APIs:

ARA Weather APIs
==================

ARA offers APIs for collecting weather data from deployment sites. At
present, we deployed weather stations and disdrometers at two
locations, Wilson Hall and Agronomy Farm, for collecting fine-grained
weather and rain related parameters. The technical specification of
devices are as follows:

* Agronomy Farm
    * Davis VantagePro2 Weather Station
    * WS100 Radar Precipitation Sensor/Smart Disdrometer

* Wilson Hall
    * Davis VantagePro2
    * OTT Parsivel² Disdrometer

Davis Vantage Pro2 provides generic weather parameters such as
temperature, pressure, humidity, wind speed, wind direction, and rain
rate while the WS100 and OTT Parsivel disdrometers provide
fine-grained weather related attributes such as rain intensity, rain
drop-size distribution and weather code (indicating snow, haze, rain,
etc.). Detailed description of weather parameters from each site are
as follows:

.. list-table:: Wilson Hall
   :align: center
   :widths: 10 10 15
   :header-rows: 1

   * - Parameter
     - Unit
     - Format
   * - DateTime
     - Timestamp with date and time
     - YYYY-MM-DD hh:mm:ss.ffff
   * - AverageParticleSpeed
     - m/s
     - | 00.000;00.000; ...;00.000;
       | **Description:** 32 items (:download:`Particle Size Table <files/OTT_Volume_Equivalent_Diameter.pdf>` with 32 classes.)
       | - The 1st item indicates the average speed of particles in Class 1 of Particle Size Table,
       | i.e., diameter 0-0.125mm.
       | - The 2nd item indicates the average speed of particles in Class 2 of Particle Size Table,
       | i.e., diameter 0.125-0.15mm.
   * - Humidity
     - Percentage
     - 00
   * - KineticEnergy
     - Joules/m\ :sup:`2` h
     - 000.000
   * - MORVisibility
     - meters
     - 00000
   * - ParticlesDetected
     - Integer count
     - 00000
   * - Pressure
     - Inches
     - 00.000
   * - RadarReflectivity
     - dBz
     - 00.00
   * - RainAbsolute
     - mm
     - 000.000
   * - RainAccumulated
     - mm
     - 0000.00
   * - RainIntensity
     - mm/hour
     - 0000.000
   * - RainRate
     - Inches
     - 00.000
   * - RawVolumeEquivalentDiameter
     - log\ :sub:`10` (1/m\ :sup:`3` mm)
     - | 000;000;...;000;
       | **Description:** 32 items (:download:`Particle Size Table <files/OTT_Volume_Equivalent_Diameter.pdf>` with 32 classes.)
       | - The 1st item indicates the **number of particles** in Class 1 of Particle Size Table,
       | i.e., diameter 0-0.125mm.
       | - The 2nd item indicates the **number of particles** in Class 2 of Particle Size Table,
       | i.e., diameter 0.125-0.15mm.
   * - SnowDepthIntensity
     - mm/hour
     - 0000.00
   * - Temperature
     - Farenheit
     - 00.0
   * - VolumeEquivalentDiameter
     - Count
     - | 000;000;...;000;
       | **Description:** 1024 items (:download:`Particle Size Table <files/OTT_Volume_Equivalent_Diameter.pdf>`, :download:`Particle Velocity Table <files/OTT_Particle_Speed.pdf>` each with 32 classes.)
       | - The 1st item represents number of particles with diameter 0-0.125mm (Class 1 of Particle
       | Size Table) and speed 0-0.1m/s (Class 1 of Particle Velocity Table).
       | - The 2nd item represents number of particles with diameter 0.125-0.25mm (Class 2 of Particle
       | Size Table) and speed 0-0.1m/s (Class 1 of Particle Velocity Table).
       | - The 33rd item represents number of particles with diameter 0-0.125mm (Class 1 of Particle
       | Size Table) and speed 0.1-0.2m/s (Class 2 of Particle Velocity Table).

   * - WeatherCode4677
     - :download:`4677 Codes (2nd Column) <files/OTT_Precipitation_SYNOP.pdf>`
     - 00
   * - WeatherCode4678
     - :download:`4678 Codes (2nd Column) <files/OTT_Precipitation_NWS.pdf>`
     - +RASN
   * - WeatherCode4680
     - :download:`4680 Codes (1st Column) <files/OTT_Precipitation_SYNOP.pdf>`
     - 00
   * - WeatherCodeNWS
     - :download:`NWS Codes (1st Column) <files/OTT_Precipitation_NWS.pdf>`
     - RLS+
   * - WindDirection
     - Angles
     - 000
   * - WindSpeed
     - kilometer/hour
     - 000


**Courtesy:** The weather codes are taken from `OTT Parsivel Weather Sensor Operating Instructions <https://www.ott.com/download/operating-instructions-present-weather-sensor-ott-parsivel2-with-screen-heating-3/>`_.

.. list-table:: Agronomy Farm
   :align: center
   :widths: 10 10 8
   :header-rows: 1

   * - Parameter
     - Unit
     - Format
   * - DateTime
     - Timestamp with date and time
     - YYYY-MM-DD hh:mm:ss.ffff
   * - Temperature
     - Farenheit
     - 00.0
   * - DrizzleParticles
     - Count
     - 0000
   * - Drops0.5-1.0mm
     - Integer count
     - 000
   * - Drops1.0-1.5mm
     - Integer count
     - 000
   * - Drops1.5-2.0mm
     - Integer count
     - 000
   * - Drops2.0-2.5mm
     - Integer count
     - 000
   * - Drops2.5-3.0mm
     - Integer count
     - 000
   * - Drops3.0-3.5mm
     - Integer count
     - 000
   * - Drops3.5-4.0mm
     - Integer count
     - 000
   * - Drops4.0-4.5mm
     - Integer count
     - 000
   * - Drops4.5-5.0mm
     - Integer count
     - 000
   * - Drops<0.5mm
     - Integer count
     - 000
   * - Drops>5.5mm
     - Integer count
     - 000
   * - HailParticles
     - Integer count
     - 000
   * - Humidity
     - Percentage
     - 000
   * - PrecipitationCumulative
     - Inches
     - 00.00000
   * - PrecipitationDifferential
     - Inches
     - 000
   * - PrecipitationIntensityHr
     - Inches/hour
     - 000
   * - PrecipitationIntensityMin
     - Inches/minute
     - 000
   * - PrecipitationParticles
     - Integer count
     - 000
   * - PrecipitationType
     - | 0: No precipitation, 60: Liquid precipitation (rain),
       | 70: Solid precipitation (snow), 40: Unspecified,
       | 67: Freezing rain, 69: Sleet, 90: Hail
     - 00
   * - Pressure
     - Inches
     - 000
   * - RainRate
     - Inches
     - 00.000
   * - SnowParticles
     - Integer count
     - 000
   * - Temperature
     - Farenheit
     - 00.000
   * - TotalDrops
     - Integer count
     - 000
   * - WeatherType
     - :download:`4680 Codes (1st Column)<files/OTT_Precipitation_SYNOP.pdf>`
     - 00
   * - WindDirection
     - Angles
     - 000
   * - WindSpeed
     - kilometer/hour
     - 000



Using the ARA Weather Library
-----------------------------------

#. Launch a container of your choice, for example, as shown in the
   :ref:`Hello World <Ara_Hello_World>` experiment.

#. Install dependencies and ARA Wireless Python library

   * Install the dependencies **git** and **pip**. ::

       # apt update
       # apt install python3-pip git

   * Install the **arawireless** Python library::
     
       # pip install git+https://gitlab.com/aralab/arasoft/arawireless.git

#. Use the following statements your Python shell or within your
   Python script. An example usage in the Python shell is shown
   below. Run the following commands in the container shell. ::

    # python3
    >>> from araweather import weather
    >>> weather_data = weather.get_current_weather(['WilsonHall'])
    >>> print(weather_data)

   You can retrieve weather data from multiple sites using the same
   function as follows: ::

    >>> weather_data = weather.get_current_weather(['WilsonHall', 'AgronomyFarm'])
    >>> print(weather_data)

   The function ``get_current_weather()`` returns the weather
   parameters at the current time in a Python dictionary. The
   extracted data can be written to a CSV file as:

    >>> weather_data = weather.get_current_weather(['WilsonHall', 'AgronomyFarm'])
    >>> weather.write_to_file(weather_data)

   The command creates a CSV file with name as location name and write
   the data to the file.
