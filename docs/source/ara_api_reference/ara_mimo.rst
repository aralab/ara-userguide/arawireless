.. _ARA_MIMO_APIs:

AraMIMO API 
==============
As mentioned in :ref:`ARA_Infrastructure`, ARA is equipped with
hardware and software components for enabling wireless research in
massive MIMO. As part of MIMO in ARA (we call as AraMIMO), we provide
APIs for monitoring Skylark applications, specifically the status of
Skylark components and links on the BS. At present, ARA provides 
monitoring, and measurement related APIs on the BS and configuration 
related APIs on the AraMIMO UEs. However, we plan to add more APIs enabling
users to configure the parameters such as frequency, bandwidth, and
gain on the BS.

AraMIMO BS API
--------------

The BS APIs are packed within the container that communicates with 
the API server running on the Skylark CU and returns the required 
information to the users. The architecture of AraMIMO BS API is shown below. 

.. image:: images/api_wrapper.PNG
    :width: 350
    :align: center

`AraRAN_Expt2 
<https://arawireless.readthedocs.io/en/latest/
ara_experiments/ara_ran_experiments.html#
experiment-2-monitoring-aramimo-wireless-link-behavior-using-cli>`_ 
provides instructions for creating and launching
containers equipped with AraMIMO APIs while the source of APIs are
available in `AraMIMO GitLab project
<https://gitlab.com/aralab/arasoft/aramimo/aramimo>`_. The container
is integrated with a CLI providing commands for monitoring
MIMO-related components and attributes. The AraMIMO CLI can be
launched by running the following command inside the container. ::

    ara-sklk-cli.py

The AraMIMO BS API currently supports following commands which can be
executed from the container. ::

	get_center_freq      Return the center frequency on all radios
	get_config           Return the current BS configuration
	get_connected_ues    Returns the list of connected UEs
	get_csi_data         Returns the CSI data
	get_main_stats       Returns the detailed system statistics
	get_mcs              Print the average MCS index for UEs
	get_phy_config       Print the PHY configuration parameters
	get_snr              Print the SNR of connected UEs
	get_ue_bus_rates     Print the bus rates in Mbps
	get_ue_data_rates    Print the data rate in Mbps
	get_ue_stats         Print the statistics of connected UEs
	help                 List the available commands and options with "help"
	history              View, run, edit, and save past commands.
	quit		     Exit from the AraMIMO CLI


Some of the commands enable users to provide options such as exporting
output to a CSV file. To check the options for each command type
``help`` before the command as shown below. ::

    help <command>

Example Scripts
---------------

Besides the CLI, the AraMIMO container includes specific scripts that
help users to automate their MIMO-related measurement. The scripts are
included in the container in the ``aramimo`` directory and the source
files are provided in the `AraMIMO GitLab project
<https://gitlab.com/aralab/arasoft/aramimo/aramimo>`_. The scripts can
be modified to suit user's experimental requirement on capturing the
data and saving output to the files in required formats. Note that
each script first establishes a socket connection to the CU to obtain
required information. Each example script is developed with the
following sequence of steps. 

#. Import required libraries
#. Create the socket connection to the CU
#. Call the specific APIs
#. Parse the returned data

**Note**: This section is not complete and will be updated as more
commands are added to the API.


AraMIMO UE API 
--------------
Similar to the BS wrapper APIs above, AraMIMO also provides APIs for 
control and configuration of the AraMIMO UEs along with measurement APIs.
The UE API architecture is a little bit different than the BS APIs since 
UE APIs provide configuration control of the UEs to the experimenters. 
Therefore, the container with UE APIs has to be launched on the particular
compute node which is connected to the UE that is reserved for the 
experiment. The architecture of the UE APIs is shown below.

.. image:: images/ue_api_diagram.png
    :width: 350
    :align: center

The API server is running on the host computer and the container is 
running the API client. The container has access to the API server on
the particular host computer which is reserved for the experiment. Some
of the APIs availble for the AraMIMO UEs are shown below. ::

	get_config           	Return the current UE configuration
	get_phy_stats	    	Returns the detailed statistics
	get_connection_status   Returns current connection status of the UE
	get_conn_uptime       	Returns the uptime of the UE
	set_close_radio         Close the Radio PHY
	set_configure_radio		Configure the radio to start transmission
	set_tx_gain				Set the Tx gain for the UE
	set_rx_gain				Set the Rx gain for the UE (Rx gain is adaptive)
	set_single_stream		Set the UE to communicate using single stream
	set_dual_stream			Set the UE to communicate using dual streams
