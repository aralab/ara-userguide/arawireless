.. _ARA_Power_APIs:

ARA Power APIs
==================

Similar to Weather APIs, ARA offers experimenters options to retrieve
and analyze the energy consumption of components involved in their
experiments or study communication systems behavior from an
energy-consumption perspective.

At present, ARA offers APIs for collecting power-related data from 4
Base Station (BS) sites and from Sandbox. At first, we provide APIs
for the overall power consumption of BS sites. We plan to add
additional APIs for the fine-grained energy measurements soon
involving options to select individual components at the sites. The
power related attributes provided by the APIs are shown in the table
below. 

.. list-table:: Power-related parameters
   :align: center
   :widths: 2 5 5
   :header-rows: 1

   * - Serial No.
     - Parameter
     - Unit
   * - 1
     - Load Amps
     - Ampere
   * - 2
     - Load VA
     - Volt-ampere
   * - 3
     - Load Watts
     - Watts


Using the ARA Power Module
--------------------------------

The **arawireless** Python library offers a single solution to generic
ARA measurement APIs including weather and power. The steps for using
power-related APIs are as follows: 

#. Launch a container of your choice, for example, as shown in the
   :ref:`Hello World <Ara_Hello_World>` experiment.

#. Install dependencies and ARA Wireless Python library

   * Install the dependencies **git** and **pip**. ::

       # apt update
       # apt install python3-pip git

   * Install the **arawireless** Python library::
     
       # pip install git+https://gitlab.com/aralab/arasoft/arawireless.git

#. Use the following instructions in either Python shell or within
   your Python script for extracting the power consumption of a
   deployment site. An example usage from the Python shell is shown
   below. Run the following commands in the container shell. ::

    # python3
    >>> from arapower import power
    >>> power_data = power.get_current_aggregate_power(['WilsonHall'])
    >>> print(power_data)

   You can retrieve weather data from multiple sites using the same
   function as follows: ::

    >>> power_data = power.get_current_aggregate_power(['WilsonHall', 'AgronomyFarm'])
    >>> print(power_data)

   .. note:: You can use site names such as *ResearchPark,
	     CurtissFarm, Boone, Gilbert,* and *ISICS* for getting
	     aggregate power consumption from respective base station sites.

   The function ``get_current_aggregate_power()`` returns the energy
   consumption at the current time in a Python dictionary.


