.. _AraOptical_APIs:

AraOptical APIs
==================

Juniper ACX710 router interface parameters for AraOptical (FSOC).

.. list-table:: Laser and Optical Power Parameters
   :align: center
   :widths: 10 10 15
   :header-rows: 1

   * - Parameter
     - Unit
     - Format
   * - laser-bias-current 
     - mA
     - | 000.000 
       | **Description:** The amount of electrical current supplied to the laser to maintain its operation.
   * - laser-output-power
     - mW
     - | 00.000
       | **Description:** The actual optical power emitted by the laser transmitter.
   * - laser-output-power-dbm
     - dBm
     - | 00.000
       | **Description:** The same optical power as above but expressed in decibels.
   * - rx-signal-avg-optical-power
     - mW
     - | 00.000
       | **Description:** The average received optical power at the transceiver.
   * - rx-signal-avg-optical-power-dbm
     - dBm
     - | 00.000
       | **Description:** The same received optical power as above but in decibel-milliwatts.

.. list-table:: Module Environmental Parameters
   :align: center
   :widths: 10 10 15
   :header-rows: 1

   * - Parameter
     - Unit
     - Format
   * - module-temperature
     - degrees C / degrees F
     - | 000
       | **Description:** The current temperature of the optical module (SFP, SFP+, QSFP, etc.).
   * - module-voltage
     - Volts
     - | 0.000
       | **Description:** The operating voltage supplied to the optical module.

.. list-table:: Alarm and Warning Thresholds
   :align: center
   :widths: 10 10 15
   :header-rows: 1

   * - Parameter
     - Unit
     - Format
   * - laser-bias-current-high-alarm-threshold
     - mA
     - | 000.000
       | **Description:** The maximum allowable bias current before an alarm triggers.
   * - laser-bias-current-low-alarm-threshold
     - mA
     - | 000.000
       | **Description:** The minimum bias current before an alarm triggers.
   * - laser-bias-current-high-warn-threshold
     - mA
     - | 000.000
       | **Description:** A warning threshold before reaching a high alarm.
   * - laser-bias-current-low-warn-threshold
     - mA
     - | 000.000
       | **Description:** A warning threshold before reaching a low alarm.
   * - laser-tx-power-high-alarm-threshold / laser-tx-power-low-alarm-threshold
     - mW
     - | 0.000
       | **Description:** Limits for the optical power output of the laser transmitter.
   * - laser-tx-power-high-alarm-threshold-dbm / laser-tx-power-low-alarm-threshold-dbm
     - dBm
     - | 0.000
       | **Description:** Limits for the optical power output of the laser transmitter but expressed in decibels.  
   * - laser-tx-power-high-warn-threshold / laser-tx-power-low-warn-threshold
     - mW
     - | 0.000
       | **Description:** Warning thresholds before reaching alarm conditions.
   * - laser-tx-power-high-warn-threshold-dbm / laser-tx-power-low-warn-threshold-dbm
     - dBm
     - | 0.000
       | **Description:** Warning thresholds before reaching alarm conditions but expressed in decibels.
   * - module-voltage-high-alarm-threshold / module-voltage-low-alarm-threshold
     - Volts
     - | 0.000
       | **Description:** Voltage limits for safe module operation.
   * - module-voltage-high-warn-threshold / module-voltage-low-warn-threshold
     - Volts
     - | 0.000
       | **Description:** Warnings for potential voltage issues.
   * - laser-rx-power-high-alarm-threshold / laser-rx-power-low-alarm-threshold
     - mW
     - | 0.000
       | **Description:** Limits for received optical power. 
   * - laser-rx-power-high-alarm-threshold-dbm / laser-rx-power-low-alarm-threshold-dbm
     - dBm
     - | 0.000
       | **Description:** Limits for received optical power, expressed in decibels.
   * - laser-rx-power-high-warn-threshold / laser-rx-power-low-warn-threshold
     - mW
     - | 0.000
       | **Description:** Early warnings for potential optical power issues. 
   * - laser-rx-power-high-warn-threshold-dbm / laser-rx-power-low-warn-threshold-dbm
     - dBm
     - | 0.000
       | **Description:** Early warnings for potential optical power issues, expressed in decibles. 


Example output: ::

        root@WilsonHall-Host-000:/# araopticalcli interface status show xe-0/0/4
        Interface: xe-0/0/4
        laser-bias-current      72.240 (unit: mA / milliamps)
        laser-output-power      1.5870 (unit: mW)
        laser-output-power-dbm  2.01 (unit: dBm)
        module-temperature      45 degrees C / 113 degrees F (unit: degrees C / degrees F)
        module-voltage  3.2250 (unit: Volts)
        rx-signal-avg-optical-power     0.0005 (unit: mW)
        rx-signal-avg-optical-power-dbm -33.01 (unit: dBm)
        laser-bias-current-high-alarm   off (unit: mA / milliamps)
        laser-bias-current-low-alarm    off (unit: mA / milliamps)
        laser-bias-current-high-warn    off (unit: mA / milliamps)
        laser-bias-current-low-warn     off (unit: mA / milliamps)
        laser-tx-power-high-alarm       off (unit: mW)
        laser-tx-power-low-alarm        off (unit: mW)
        laser-tx-power-high-warn        off (unit: mW)
        laser-tx-power-low-warn         off (unit: mW)
        module-temperature-high-alarm   off (unit: degrees C / degrees F)
        module-temperature-low-alarm    off (unit: degrees C / degrees F)
        module-temperature-high-warn    off (unit: degrees C / degrees F)
        module-temperature-low-warn     off (unit: degrees C / degrees F)
        module-voltage-high-alarm       off (unit: Volts)
        module-voltage-low-alarm        off (unit: Volts)
        module-voltage-high-warn        off (unit: Volts)
        module-voltage-low-warn         off (unit: Volts)
        laser-rx-power-high-alarm       off (unit: mW)
        laser-rx-power-low-alarm        on  (unit: mW)
        laser-rx-power-high-warn        off (unit: mW)
        laser-rx-power-low-warn         off (unit: mW)
        laser-bias-current-high-alarm-threshold 120.000 (unit: mA / milliamps)
        laser-bias-current-low-alarm-threshold  10.000  (unit: mA / milliamps)
        laser-bias-current-high-warn-threshold  110.000 (unit: mA / milliamps)
        laser-bias-current-low-warn-threshold   20.000  (unit: mA / milliamps)
        laser-tx-power-high-alarm-threshold     3.1620  (unit: mW)
        laser-tx-power-high-alarm-threshold-dbm 5.00    (unit: dBm)
        laser-tx-power-low-alarm-threshold      0.5010  (unit: mW)
        laser-tx-power-low-alarm-threshold-dbm  -3.00   (unit: dBm)
        laser-tx-power-high-warn-threshold      2.5110  (unit: mW)
        laser-tx-power-high-warn-threshold-dbm  4.00    (unit: dBm)
        laser-tx-power-low-warn-threshold       0.6310  (unit: mW)
        laser-tx-power-low-warn-threshold-dbm   -2.00   (unit: dBm)
        module-temperature-high-alarm-threshold 88 degrees C / 190 degrees F 
        module-temperature-low-alarm-threshold  -43 degrees C / -45 degrees F
        module-temperature-high-warn-threshold  85 degrees C / 185 degrees F
        module-temperature-low-warn-threshold   -40 degrees C / -40 degrees F
        module-voltage-high-alarm-threshold     3.600 (unit: Volts)
        module-voltage-low-alarm-threshold      3.000 (unit: Volts)
        module-voltage-high-warn-threshold      3.500 (unit: Volts)
        module-voltage-low-warn-threshold       3.100 (unit: Volts)
        laser-rx-power-high-alarm-threshold     0.1996 (unit: mW)
        laser-rx-power-high-alarm-threshold-dbm -7.00  (unit: dBm)
        laser-rx-power-low-alarm-threshold      0.0026 (unit: mW)
        laser-rx-power-low-alarm-threshold-dbm  -25.85 (unit: dBm)
        laser-rx-power-high-warn-threshold      0.1260 (unit: mW)
        laser-rx-power-high-warn-threshold-dbm  -9.00  (unit: dBm)
        laser-rx-power-low-warn-threshold       0.0040 (unit: mW)
        laser-rx-power-low-warn-threshold-dbm   -23.98 (unit: dBm)