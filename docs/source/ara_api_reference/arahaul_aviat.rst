Aviat API
===============


For microwave and millimeter-wave based wireless experiments using
Aviat radios, we provide a set of wrapper APIs for configuration as
well as measurement. The APIs currently supports only basic
commands. In future, we plan to enable additional configuration
related APIs will to provide more flexibility to change the radios
configuration at more finer level. Following a containerized resource
provisioning of ARA, the APIs runs inside containers.  The APIs
interact with Aviat radios and for configuration and querying the
status of radio/link. More information on launching containers for
Aviat-based experiments can be found in :ref:`AraHaul_Experiments`. 


Measurement APIs
------------------------------

The APIs are used to measure the performance-related parameters of
Aviat radios and the mmWave/Microwave links. ::

  aviatcli carrier show capabilities <Carrier name>               Checks the radio-carrier capabilities from local-end
  aviatcli carrier show status  <Carrier name>                    Checks the local node's real-time radio-carrier status
  aviatcli radio show capabilities                                Checks the radio-link capabilities from local-end
  aviatcli radio show status                                      Checks the local node's real-time radio-link status
  aviatcli carrier show capabilities <Carrier name> --remote      Checks the radio-carrier capabilities from remote-end
  aviatcli carrier show status  <Carrier name> --remote           Checks the remote node's real-time radio-carrier status
  aviatcli radio show capabilities --remote                       Checks the radio-link capabilities from remote-end
  aviatcli radio show status --remote                             Checks the remote node's real-time radio-link status


Configuration APIs
--------------------------------

Configuration APIs are used to change the various radio-carrier
parameters of Aviat radios and the mmWave/Microwave link ::

    aviatcli carrier enable <Carrier name>                  Enables the specific radio-carrier at local-end
    aviatcli carrier enable <Carrier name> --remote         Enables the specific radio-carrier at remote end
    aviatcli carrier configure all [options]                Configures attributes at both local and remote ends

..
     show radio-carrier capabilities       To check the radio-carrier capabilities for each carrier
     show radio-link capabilities          To check the radio-link capabilities for each radio
     show radio-carrier status             To check the real-time radio-carrier status
     show radio-link status                To check the real-time radio-link status



