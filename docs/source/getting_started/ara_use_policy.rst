.. _ARA_Use_Policy:

ARA Acceptable Use Policy
================================================


Acceptable Use Policy
-------------------------

This acceptable use policy (the "AUP") applies to the use of the ARA
wireless living lab (or “ARA”, "Platform"). ARA is a testbed that is
designed to allow researchers from academia, industry, and government
to experiment with wireless and mobile network protocols and
technologies with a focus on rural and agricultural wireless
experiments. Any individual accessing the Platform (a "User") may do
so only for a Permitted Use, as defined below, and only as otherwise
permitted under this AUP. The ARA administrators may terminate access
to the Platform for individuals or organizations who violate any of
these terms; whose use of the Platform, in the sole judgment of the
ARA administrators, poses a threat to ARA’s access to or right to use
licensed or unlicensed spectrum or the safety, security, or integrity
of the Platform, other Users, or any third parties; or whose use of
the Platform is not aligned with the mission of ARA.

User Agreement
--------------------

As a condition of using the Platform, I agree to the following terms
and conditions on behalf of myself and any organization on whose
behalf I am using the Platform.  


**Permitted uses.** Unless I am accessing the Platform pursuant to a
separate, written agreement setting forth the nature of my use of the
Platform, I understand that I am allowed to use the Platform only for
non-commercial purposes or for short-term evaluation of the Platform
to determine suitability for commercially related research (each a
"Permitted Use").

**Prohibited uses.** I understand that it is my responsibility to
comply with all laws applicable to the use of the Platform. Parts of
the Platform are capable of sending and receiving radio-frequency
communications over the air, in a public environment. I understand
that use of these frequencies is regulated by the US Federal
Communications Commission and that transmission of signals over these
frequencies is governed by State and Federal laws and regulations. I
also understand that the use of these frequencies by ARA may be
limited or constrained by operating or other agreements with
third-party holders of spectrum licenses. I acknowledge that it is my
responsibility to know the terms and requirements of any applicable
government regulations, as well as the terms and conditions that ARA
is required to comply with in regard to any third parties insofar as
they are provided to me by ARA, and to abide by them in my use of the
Platform. These terms and requirements include the licensing
agreements of the software deployed in the Platform, such as
OpenAirInterface, srsRAN, and GNU Radio. I will not use the Platform
for any purpose that is unlawful or otherwise inconsistent with
applicable laws or regulations or the contractual limitations that ARA
is subject to.

In addition, I will not use the Platform for any purpose other than a
Permitted Use (each a "Prohibited Use"). By way of example (and not
limitation), the following uses of the Platform shall constitute
Prohibited Uses under this AUP:

 * Any attempt to manipulate or circumvent the Platform's controls
   related to user access, including sharing account credentials,
   revealing my true identity or that of my collaborators, or
   accessing the system without permission;

 * Any use that results in an individual being abused, threatened,
   harassed, or intimidated;

 * Any use that would constitute a crime or violation of law,
   including using the Platform to commit fraud or make material
   misrepresentations, or in furtherance of any criminal activity, or
   in a manner that violates FCC regulations;

 * Accessing or attempting to access the Platform to copy, reverse
   engineer, decompile or otherwise misappropriate any proprietary
   aspect of the Platform, or to attempt to violate the intellectual
   property rights of others, including the Iowa State University,
   other Platform users, or those who have provided equipment,
   software, or intellectual property for use as part of the Platform
   (each a "Platform Collaborator");

 * Any use of the Platform that results in a violation of the privacy
   rights of another User or reasonable expectations of privacy of any
   third party;

 * Any use that results in a violation of any other provision of this
   AUP (or such modified version of the AUP as may apply to the use of
   the Platform at the time of such use);

 * Any use that is not consistent with a request by Platform
   Administrators related to the safety, security, or integrity of the
   Platform or research supported by or related to the Platform;

 * Any use for a non-evaluative commercial purpose or to provide
   services to third parties, including, for example, provide
   communications services to individuals or organizations;

 * Any use that is in contravention of the limitations on the use of
   spectrum used by ARA that is contrary to any applicable law,
   regulation, or contractual limitation on the use of spectrum by the
   ARA Platform.

Privacy
---------

I understand that ARA administrators may collect and use information
about me and my usage of the Platform in order to maintain, improve,
and administer the Platform; provide platform support; verify my
eligibility to use the Platform; and as otherwise permitted or
required by applicable law. To the extent permitted by applicable law,
including laws related to access to public records, ARA Administrators
will employ reasonable safeguards to protect the identities of Users
and the confidentiality of their data. ARA administrators may attempt
to provide isolated computer and storage resources for a limited
amount of time to facilitate User efforts to maintain confidentiality
of data, but it is the User’s responsibility to arrange for the
provision of such resources (e.g., via the web portal of ARA). ANY
SUCH RESOURCES ARE OFFERED ON AN AS-IS BASIS, WITHOUT GUARANTEE OF ANY
KIND. RF communications over the air, cannot, by their nature, be
protected from interception by third parties; Users must take
appropriate precautions with any sensitive data that they
transmit. ARA does not represent its ability to handle any User data
in accordance with any particular specialized legal, accreditation, or
industry requirements for data privacy or security (such as the Health
Insurance Portability and Accountability Act ("HIPAA") and Users shall
not use the Platform to store or transmit data if such requirement
would apply.


Data Storage
----------------

ARA does not provide guarantees with respect to the reliability of the
storage it provides; it is Users' responsibility to make copies of
data that is important to them.

Other Circumstances 
-----------------------

The ARA administrators reserve the right to exercise judgment with
respect to the use of the Platform and its resources and to restrict
or terminate access as they see fit.

Consequences 
---------------------

In addition to the other consequences described herein, violation of this AUP may result in any or all of the following: 

 * shutting down a User's access to ARA; 
 * shutting down a User's access to the radio frequencies used by ARA; 
 * terminating a User's experiment, including deletion of data associated with it; 
 * disabling User accounts or organizational access; 
 * informing an organization about its User's violations of this policy; and/or 
 * referral to applicable law enforcement authorities.

Limitation of Liability
----------------------------

The Platform is an experimental platform offered for research purposes
only. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IOWA STATE
UNIVERSITY (the “UNIVERSITY”) DISCLAIMS ANY AND ALL LIABILITY FOR ANY
HARMS ARISING OUT OF YOUR USE OF THE PLATFORM AND YOU HEREBY WAIVE ANY
CLAIMS YOU MAY HAVE RELATED TO SUCH HARMS AGAINST THE UNIVERSITY AND
PLATFORM COLLABORATORS EACH AND ALL OF WHOM ARE INTENDED THIRD-PARTY
BENEFICIARIES OF THIS AUP. You will indemnify, defend, and hold
harmless the University and the Platform Collaborators against any and
all claims, damages, costs, and expenses (including attorney's fees)
(collectively "Damages") to the extent such Damages arise from your
use of the Platform.

To report a suspected violation of this policy, please email
support@arawireless.org and CC ara-support@iastate.edu.

**Version:** 1.0

**Acknowledgment:** portions of this AUP document are adopted from the `Powder AUP`_ and `AERPAW AUP`_.

.. _Powder AUP: https://powderwireless.net/aup

.. _AERPAW AUP: https://sites.google.com/ncsu.edu/aerpaw-wiki/aerpaw-user-manual/2-experiment-lifecycle-workflows/2-5-acceptable-use-policy-aup?authuser=0
