.. _ARA_Register_User_Account:

Register User Account
====================================
To access ARA resources, a new user needs to first join an existing project or 
create a new project in ARA. In what follows, we explain the concept of Project 
in ARA and the processes by which a new project can be created and a new user 
can join an existing project. 



Projects in ARA
-----------------
Projects in ARA provide the environment for users to use the ARA
resources. A project in ARA consists of a group of users along with
options for them to manage ARA resources such as wireless, network, compute,
and storage resources. As detailed below, a typical project in ARA has two types
of users: (1) a project administrator and (2) project members.



ARA Users
-----------------

The roles of ARA users are classified into two types: **(1) Project Administrator** and **(2) Project Member**.

  * **Project Administrator:** A project administrator can be a person
    who can play the role of administrator for the activities in ARA
    on behalf of a group. A typical example of a project administrator
    can be the Principal Investigator (PI) or a person leading a
    research group, a professor teaching a course involving wireless
    research, or the manager of a research team in industry. In ARA, a
    project administrator is allowed to manage all aspects of a
    project including adding/deleting users to the project, assigning
    roles to its users, creating/deleting networks, creating/deleting
    security groups, creating/deleting leases, and
    creating/stopping/deleting containers. To get a Project
    Administrator access to ARA, users can submit a request in the
    `ARA User Sign-up Form`_. Once the request is approved by the ARA
    team, the user gets a confirmation email so that they can login to
    the portal and add project members. Instructions on adding project
    members can be found in :ref:`ARA User Management
    <User_Management>`.

.. _ARA User Sign-up Form: https://forms.gle/frpLBmtTMvNptrEEA

  * **Project Member:** A project member in ARA has the rights to
    execute experiments which includes activities such as
    reserving resources, creating containers on the reserved
    resources, and executing experiments in the containes created. In order to
    get a project member access to the ARA platform, a user can send a
    request to the related project administrator. In case if you are a
    stand-alone independent researcher or you are not working under a
    person who plays the role of *project administrator* in ARA,
    contact us at e2@arawireless.org so that we may consider the
    request.
