.. _ARA_FAQ:

FAQ
============

* :ref:`FAQ_ARA_General`

  #. :ref:`I am new to ARA, where and how can I get started? <FAQ_New_User_Information>`

* :ref:`FAQ_ARA_Portal`

  #. :ref:`Unable to ping/SSH to the container from ARA jumpbox <FAQ_Unable_to_Ping_SSH>`
  #. :ref:`Unable to create container <FAQ_Unable_to_Create_Container>`

* :ref:`FAQ_AraRAN`

  #. :ref:`Permission Denied while running gNB or UE <FAQ_Permission_Denied_SDR>`
  #. :ref:`Crash/core dump in the nr-softmodem command <FAQ_nrsoftmodem_crash>`
  #. :ref:`UE not connecting to gNB <FAQ_UE_not_Connecting_to_gNB>`
  #. :ref:`Skylark: KeyError: 'id' <FAQ_Skylark_Key_Error>`

* :ref:`FAQ_AraHaul`

  #. :ref:`Inconsistent iPerf Server <FAQ_Optical_iperf_Error>`

.. _FAQ_ARA_General:

ARA
----

  .. _FAQ_New_User_Information:

#. I am new to ARA, where and how can I get started?

   As a new user, explore the `ARA webpage
   <https://arawireless.org/>`_ for a high-level information on
   ARA. For specifics on ARA testbed capabilities, visit the
   :ref:`ARA_Capabilities` page. If you are ready to explore ARA, do
   not wait to :ref:`register your user account
   <ARA_Register_User_Account>` and on confirming your registration,
   start with :ref:`ARA Hello World experiment <ARA_Hello_World>` to
   get a first-level hands-on experience of ARA experiments
   workflow. Further, you can explore specific domains of wireless
   experiments such as :ref:`AraRAN <AraRAN_Experiments>` and
   :ref:`AraHaul <AraHaul_Experiments>`.

   For getting a glimpse of ARA vision, design, and implementation
   refer the following manuscripts:

     * `ARA: A Wireless Living Lab Vision for Smart and Connected
       Rural Communities
       <https://www.ece.iastate.edu/~hongwei/group/publications/ARA_Vision.pdf>`_

     * `Design and Implementation of ARA Wireless Living Lab for Rural
       Broadband and Applications <https://arxiv.org/pdf/2408.00913>`_


.. _FAQ_ARA_Portal:

ARA Portal
-------------

   .. _FAQ_Unable_to_Ping_SSH:

#. **Unable to ping/SSH to the container from ARA jumpbox**

   **Reason:** The security group rules of your project is blocking
   the ICMP and SSH traffic. 

   **Solution:** Go to the security groups page via *Project ->
   Network -> Security Groups* on the dashboard. Click on the *Manage
   Rules* button of the **default** security group. In the redirected
   page, you can see the existing security group rules. You need to
   add two rules with the following attributes:

   **Rule 1**
     * Rule: **All ICMP**
     * Description: Optional
     * Direction: **Ingress**
     * Remote: **CIDR**
     * CIDR: **0.0.0.0/0**
   **Rule 2**
     * Rule: **SSH**
     * Description: Optional
     * Remote: **CIDR**
     * CIDR: **0.0.0.0/0**

   .. _FAQ_Unable_to_Create_Container:


#. **Unable to create container**

   **Reason:** The Docker daemon may face some issues while
   downloading the image from the Docker Hub due to any kind of
   network disruptions in the wireless management channel. This error
   may occur at UE sites deployed at remote locations. The status of
   container may show as **Error**.

   **Solution:** Delete the existing container and create a new
   container. Note that the container with error will be deleted
   automatically on an error. 


.. _FAQ_AraRAN:

AraRAN
-----------------

   .. _FAQ_Permission_Denied_SDR:

#. **Permission denied while running gNB or UE**

   **Reason:** The Docker container does not have permission to access
   the SDR.

   **Solution:** Stop (no need to delete the container) the respective
   container from the *Project -> Containers* page and start the
   container again. In case, restarting the container does not help,
   you can delete and create the container again.

   .. _FAQ_nrsoftmodem_crash:

#. **Crash/core dump in the nr-softmodem command**

   **Solution:** Re-run the `nr-softmodem` command. It may happen that
   you may need to run the command 2-3 times. 

   .. _FAQ_UE_not_Connecting_to_gNB:

#. **UE not getting ``oaitun_ue1`` interface**

   **Reason:** The PDU session may have not yet been established between
   UE and the 5G core network.

   **Solution:** Restart the 5G core network but running the following
   command in the core network container. ::
      
      # docker compose down
      # docker compose up -d
   
#. **UE not connecting to gNB**

   **Reason:** When the UE starts, it searches for the signals
   from the BS and begins a procedure to attach. The procedure takes a
   few seconds, especially when the UE is far away from the BS. 

   **Solution:** In case the UE is not getting connected to the gNB
   after executing the command ``nr-softmodem`` (Step 12 of Experiment
   6 of AraRAN experiments) for 30 seconds, we suggest to stop the
   command (using Ctrl+C) and run it again. It may happen that you
   need to run a few times to get the UE connected to the BS.

   .. _FAQ_Skylark_Key_Error:   

#. **Skylark Key Error during Measurements**

   **Reason:** AraMIMO measurements scripts runs into ``Key Error``
   issue when collecting data. The Skylark application does not 
   return the control from the call to the data through the object
   ``client`` and the script keeps listening for return data. 

   **Solution:** Stop the script with ``CTRL + C`` and rerun the 
   script. If it keeps running into this issue, reduce the number 
   of iterations e.g. 10 or 15 or 30. 

.. _FAQ_AraHaul:

AraHaul
-----------

   .. _FAQ_Optical_iperf_Error:

#. **iPerf Server Inconsistency Error during Measurements**

   **Reason:** At times the unstable link results in packet loss,
   which makes the *iPerf* control channel inconsistent. In such
   cases, the server side produces the continuous output as shown
   below. 

   .. figure:: images/Optical_iperf_Server_Error.png
      :align: center

   **Solution:** Stop the server with ``CTRL + C`` and restart the
   server using the following command. ::

     # iperf3 -s 

   .. _FAQ_Bad_File_Descriptor_iperf:
   
#. **Bad File Descriptor**

   **Reason:** Due to unstable link between the client and server,
   *iPerf* may not be able to establish the connection and produces
   the following error.

   .. figure:: images/Bad_File_Descriptor_iperf.png
      :align: center

   **Solution:** Restart the *iPerf* server and client. 

   
