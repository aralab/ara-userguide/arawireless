ARA Software Architecture
=============================

ARA software framework, we call **AraSoft**, is derived from popular
`OpenStack <https://www.openstack.org/>`_ cloud operating system with
container-mode resource provisioning, the philosophy adopted by the
`CHI\@Edge <https://chameleoncloud.gitbook.io/chi-edge>`_ spin of
`Chameleon Cloud <https://www.chameleoncloud.org/>`_.  Along with the
typical resources such as compute and storage, ARA introduces wireless
resources (including cellular base stations, user equipment, and
long-range wireless backhaul) to the cloud platform for experimenters
to conduct advanced wireless research. The basic architecture of
AraSoft is shown below.

.. image:: images/Software_Architecture.png
     :width: 900
     :align: center
|

As far as the deployment is concerned, ARA includes a data center
(with compute and storage resources) and different sites equipped with
wireless resources (such as RAN base stations and user equipment, as
well as wireless backhaul). Data center maintains the ARA controller
providing a web-portal where the user logs in, reserves the resources
(compute, storage, base stations, and user equipment), and performs
the experiments. In addition to the controller, data center is
equipped with dedicated compute nodes for users to perform computation
operations or to run experiments that do not require specific type of
nodes. Along with the compute, ARA is equipped with storage nodes to
provide users an additional space for storing their data.

Each ARA site consists of a single server or multiple servers hosting
the wireless devices including RAN and wireless backhaul radios. When
a user reserves a resource, say base station, the associated host
computer is allocated so that the user can launch containers and
access the wireless resource. Further, a console access is provided to
the container via the portal or via Secure Shell (SSH) protocol using
the floating IP feature as discussed in the :ref:`Hello World
Experiment <ARA_Hello_World>`. On accessing the container, the user
can execute their experiments. The workflow of experiment is shown in
the figure below.

.. image:: images/Flow_Diagram.png
     :align: center

|
ARA controller communicates to all physical resources via the
management channel (shown in Blue color in the figure above) through
optic fiber or cellular wireless channel. Controller uses the
management channel to provide users the access to their containers. In
addition, by default, the containers themselves communicate via
virtual networks created over this management channel. For example, if
you launch Container-A at Curtiss Farm base station and Container-B at
a Curtiss Farm user equipment, they communicate via a virtual network
over the management channel. On the contrary, if you run a 5G protocol
stack on the containers (say gNB at Container-A and UE software at
Container-B), they establish a wireless link via the data channel
(shown in Red color) and use that network for communication. As shown
in the figure, ARA is equipped with long-range wireless backhaul
communication between base stations (i.e., between Wilson Hall and
Agronomy Farm in the current deployment) and users are allowed to
perform their experiment on the backhaul similar to any access network
experiments. The experiment data from base station or user equipment
can be transferred to a container in the data center compute nodes for
further processing.  







