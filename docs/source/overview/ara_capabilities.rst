.. _ARA_Capabilities:

Current ARA Capabilities
=============================

ARA provides capabilities for researchers to perform advanced wireless
experiments including RAN experiments and backhaul
experiments. Experimenters can reserve the deployed resources (listed
below) via the ARA portal and execute their experiments.


.. _ARA_Capabilities_RAN:

ARA Radio Access Networks
-------------------------------

As far as RAN is concerned, ARA deploys:

 1. USRP SDR-based Base Stations (BSes) and User Equipment (UEs)
 2. Skylark BSes and Customer Premises Equipment
    (CPEs)
 3. Ericsson BSes and Commercial-Off-The-Shelf (COTS) UEs
 4. ARA sandbox

.. _ARA_Capabilities_SDR:

SDR-capable Resources
~~~~~~~~~~~~~~~~~~~~~~~~~

.. list-table:: 
   :widths: 10 5 10
   :header-rows: 1
   :align: center

   * - Resource Type
     - Count
     - Components Used
   * - Base Station
     - 4
     - 3 x USRP NI N320 at each BS
   * - User Equipment
     - 11
     - 1 x USRP NI B210

ARA provides pre-built containers for performing experiments on USRP
SDR platforms. Detailed specification of SDR devices can be found in
:ref:`SDR_Detailed_Spec`. Refer to Experiments :ref:`1
<AraRAN_Experiment_Gnuradio>`, :ref:`4
<AraRAN_Experiment_Spectrum_Sensing>`, and :ref:`6
<AraRAN_Experiment_OAI_Outdoor>` of the :ref:`AraRAN
experiments<AraRAN_Experiments>` in the user manual for SDR-related
experiments.

.. _ARA_Capabilities_Skylark:

Skylark RAN Resources
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. list-table:: 
   :widths: 10 5 10
   :header-rows: 1
   :align: center

   * - Resource Type
     - Count
     - Components Used
   * - Base Station
     - 1
     - DU + CU + 3 RUs
   * - Customer Premises Equipment
     - 7
     - 1 x Skylark Faros v2 

The Skylark BS-CPE links are already established and users are
provided with APIs only to measure different statistics from the
system. Detailed specification of Skylark can be found
:ref:`here<Skylark_Detailed_Spec>`. The APIs are containerized and
made available to the users via DockerHub. Refer to Experiments
:ref:`2 <AraRAN_Experiment_Skylark_Link_Behavior>` and
:ref:`3<AraRAN_Experiment_Skylark_CSI>` of the :ref:`AraRAN
experiments<AraRAN_Experiments>` in the user manual for Skylark-based
mMIMO experiments.

.. _ARA_Capabilities_Ericsson:

Ericsson Resources
~~~~~~~~~~~~~~~~~~~~~~~

In addition to SDR and Skylark, ARA offers Ericsson BSes at four BS
sites and COTS UEs in each UE. The following table summarizes the
current Ericsson deployments in ARA.


.. list-table:: 
   :widths: 10 5 10
   :header-rows: 1
   :align: center

   * - Resource Type
     - Count
     - Components Used
   * - Ericsson Base Station
     - 4
     - n77 AIR6419 (midband) and n261 AIR5322 (mmWave)
   * - COTS UEs
     - 8
     - Quectel RG530F


ARA Sandbox
~~~~~~~~~~~~~~~~

ARA sandbox provides an indoor environment for performing RAN
experiments. Detailed specification of NI B210 is described
:ref:`here<SDR_B210_Detailed_Spec>`. The components in the sandbox are
listed in the following table. We include :ref:`Experiment
5<AraRAN_Experiment_OAI_Indoor>` under the :ref:`AraRAN
experiments<AraRAN_Experiments>` in the user manual for an example
experiment in the sandbox.

.. list-table:: 
   :widths: 5 5 5 
   :header-rows: 1
   :align: center

   * - Resource Type
     - Count
     - Components Used
   * - SDR
     - 50
     - USRP NI B210


.. _ARA_Capabilities_Backhaul:

ARA X-Haul Networks
--------------------------

ARA deploys long-range, high capacity wireless link that connect
between BS sites. In Phase-1, the backhaul is deployed between Wilson
Hall and Agronomy Farm. A summary of the backhaul link is provided in
the following table.

.. list-table:: 
   :widths: 5 5 5 10
   :header-rows: 1
   :align: center

   * - Resource Type
     - Count
     - Components Used
     - Frequency of operation
   * - Backhaul link
     - 1
     - 2 x :ref:`Aviat 4800<Aviat_Detailed_Spec>`
     - Microwave (11 GHz) and millimeter wave (80 GHz)

Similar to Skylark, we provide APIs for configuring the link
attributes (such as bandwidth and modulation) and measuring the link
characteristics. Refer to :ref:`Experiment
1<AraHaul_Experiment_Link_Configuration>` of
:ref:`AraHaul_Experiments` for an example experiment of AraHaul.


.. _ARA_Capabilities_Compute:

ARA Compute and Edge Resources
---------------------------------------

For computationally intensive experiments, ARA offers compute and edge
nodes. Similar to other resources, such nodes can be reserved and used
with the help of containers. The list of compute resources enabled in
ARA is summarized as follows:

.. list-table:: 
   :widths: 5 5 5 10
   :header-rows: 1
   :align: center

   * - Resource Type
     - Count
     - Site
     - Components Used
   * - Compute Node
     - 2
     - Data Center
     - Dell R750 Server
   * - Edge Node
     - 2
     - Agronomy Farm and Research Park
     - Dell R750 Server with NVIDIA Quadro RTX 5000



.. _ARA_Capabilities_Weather_APIs:

ARA Weather Service
--------------------------

In order to enable weather-focused studies on the performance of
communication systems, ARA offers a weather station service that
provides users the information on the real-time weather conditions of
the deployment sites. At present, we install generic weather stations
(to sense the basic weather parameters including temperature,
atmospheric pressure, rain rate, wind speed, wind direction, and
humidity) as well as disdrometers (to sense fine-grained rain-related
attributes including rain intensity, particle size distribution, and
weather code). ARA provides Python-APIs for extracting the weather
information for experiments. Users can use them as standalone programs
or integrate the APIs in their experiment applications. More
information on the weather related APIs can be found
:ref:`here<ARA_Weather_APIs>`.


.. _ARA_Capabilities_Power_APIs:

Power Measurement Service
----------------------------

ARA offers APIs for users to extract the power consumption from base
station sites to analyze the aspects of energy utilization. Such
studies may help designing the future green communication systems. The
power-related APIs follow the same style of weather APIs so that users
can extract the power consumption as a whole from a BS site or from
individual components at the site. More information on the
power-related APIs can be found :ref:`here<ARA_Power_APIs>`.


