Citing ARA
====================

For studies using ARA as an experimental infrastructure, it is
encouraged that the associated publications cite ARA to raise
awareness of ARA and its capabilities, thereby helping the community
use ARA for advanced wireless research. The primary reference is as
follows:


*Taimoor Ul Islam, Joshua Ofori Boateng, Md Nadim, Guoying Zu, Mukaram
Shahid, Xun Li, Tianyi Zhang, Salil Reddy, Wei Xu, Ataberk Atalar,
Vincent Lee, Yung-Fu Chen, Evan Gosling, Elisabeth Permatasari, Christ
Somiah, Zhibo Meng, Sarath Babu, Mohammed Soliman, Ali Hussain, Daji
Qiao, Mai Zheng, Ozdal Boyraz, Yong Guan, Anish Arora, Mohamed Selim,
Arsalan Ahmad, Myra B. Cohen, Mike Luby, Ranveer Chandra, James Gross,
Hongwei Zhang*, `Design and Implementation of ARA Wireless Living Lab
for Rural Broadband and Applications
<https://doi.org/10.48550/arXiv.2408.00913>`_, arXiv:2408.00913, 2024.


**BibTeX** 

.. code-block::

   @misc{ARA:Design-Impl,
   title={Design and Implementation of {ARA} Wireless Living Lab for Rural Broadband and Applications},
   author={Taimoor Ul Islam and Joshua Ofori Boateng and Md Nadim and Guoying Zu and Mukaram Shahid and Xun Li and Tianyi Zhang and Salil Reddy and Wei Xu and Ataberk Atalar and Vincent Lee and Yung-Fu Chen and Evan Gosling and Elisabeth Permatasari and Christ Somiah and Zhibo Meng and Sarath Babu and Mohammed Soliman and Ali Hussain and Daji Qiao and Mai Zheng and Ozdal Boyraz and Yong Guan and Anish Arora and Mohamed Selim and Arsalan Ahmad and Myra B. Cohen and Mike Luby and Ranveer Chandra and James Gross and Hongwei Zhang},
   year={2024},
   eprint={2408.00913},
   archivePrefix={arXiv},
   primaryClass={cs.NI},
   url={https://arxiv.org/abs/2408.00913},
   }

   @inproceedings{ARA:vision,
   author = "{Hongwei Zhang and Yong Guan and Ahmed Kamal and Daji Qiao and Mai Zheng and Anish Arora and Ozdal Boyraz and Brian Cox and Thomas Daniels and Matthew Darr and Doug Jacobson and Ashfaq Khokhar and Sang Kim and James Koltes and Jia Liu and Michael Luby and Larysa Nadolny and Joshua Peschel and Patrick Schnable and Anuj Sharma and Arun Somani and Lie Tang}",
   title = {{ARA}: A Wireless Living Lab Vision for Smart and Connected Rural Communities},
   booktitle = "ACM WiNTECH",
   year = “2021”
   }

Other technical references of ARA can be found `here <https://arawireless.org/tech-docs/>`_. 
